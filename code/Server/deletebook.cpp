#include "deletebook.h"
#include "ui_deletebook.h"

#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QFile>
#include"config.h"
deletebook::deletebook(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::deletebook)
{
    ui->setupUi(this);
}

deletebook::~deletebook()
{
    delete ui;
}

void deletebook::on_pushButton_clicked()
{
    ui->listWidget->clear();
    if(ui->lineEdit->text() == NULL)
    {
        QMessageBox::information(this,"提示","请填入输入框！");
        return;
    }
    QSqlQuery query(D.getdb());
    QString code = "%"+bookname+"%";
    query.prepare("SELECT * FROM main.xuptlibrary WHERE BookName LIKE :BookName");
    query.bindValue(":BookName",code.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        ui->lineEdit->clear();
        while(query.next())
        {
            bookname = query.value(0).toString();
            picture = query.value(2).toString();
            ui->listWidget->addItem(bookname+":[封面:"+picture+"]");
        }
    }else
    {
        qDebug()<<"搜索失败！["<<query.lastError().text()<<"]";
    }
}


void deletebook::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
//    DELETE FROM "main"."xuptlibrary" WHERE rowid = 3
    bookname = item->text().section(":",0,0);
    Q_UNUSED(item);
    QMessageBox::StandardButton bt = QMessageBox::question(this,"删除","确认删除此数据吗？",QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    if(bt == QMessageBox::Yes)
    {
        QSqlQuery query(D.getdb());
        query.prepare("DELETE FROM main.xuptlibrary WHERE BookName = :BookName");
        query.bindValue(":BookName",bookname.toUtf8());
        bool ok = query.exec();
        if(ok)
        {
            qDebug()<<"成功删除数据！";
            ui->listWidget->clear();
            emit updatebook();
        }else
        {
            qDebug()<<"数据删除失败！["<<query.lastError().text()<<"]";
        }
        QString p = pictureUrl+"/"+picture;
        qDebug()<<p;
        if(QFile::remove(p))
        {
            qDebug()<<"成功删除封面！";
        }
    }
    if(bt == QMessageBox::Yes)
    {
        ui->listWidget->clear();
    }

}


void deletebook::on_lineEdit_textChanged(const QString &arg1)
{
    bookname = arg1;
}

