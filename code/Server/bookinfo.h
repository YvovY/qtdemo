#ifndef BOOKINFO_H
#define BOOKINFO_H

#include <QDialog>
#include <QListWidgetItem>
#include <QSqlRecord>
#include"database.h"
namespace Ui {
class bookinfo;
}

class bookinfo : public QDialog
{
    Q_OBJECT
private:
    DataBase D;
    Ui::bookinfo *ui;
    QSqlRecord record;
    QString booknamedefalt = NULL;//原书名
    QString bookname = NULL;//书名
    QString isbncode = NULL;//isbn数
    QString bookshelf = NULL;//书架
    QString author = NULL;//作者
    QString bookroom = NULL;//书库
    QString publisher = NULL;//出版社
    QString totalnum = NULL;//总数量
    QString counnum = NULL;//现有数量
    QString floor = NULL;//楼层
    QString jianjie = NULL;//简介
    QString pictureDirss = NULL;//选择的图片地址
    QString pictures = NULL;//图片地址
    QString booktype = NULL;//书籍类型
public:
    explicit bookinfo(QWidget *parent = nullptr);
    ~bookinfo();
private:
    void settext();
signals:
    void updatebook();
private slots:
    void on_lineEdit_textChanged(const QString &arg1);
    void on_pushButton_clicked();
    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_bookname_textChanged(const QString &arg1);
    void on_isbn_textChanged(const QString &arg1);
    void on_author_textChanged(const QString &arg1);
    void on_publisher_textChanged(const QString &arg1);
    void on_floor_textChanged(const QString &arg1);
    void on_bookroom_textChanged(const QString &arg1);
    void on_bookshelf_textChanged(const QString &arg1);
    void on_totalnum_textChanged(const QString &arg1);
    void on_currentnum_textChanged(const QString &arg1);
    void on_jianjie_textChanged();
    void on_picture_clicked();
    void on_btok_clicked();
    void on_btcel_clicked();
    void on_BookType_textChanged(const QString &arg1);
};

#endif // BOOKINFO_H
