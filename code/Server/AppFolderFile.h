#ifndef APPFOLDERFILE_H
#define APPFOLDERFILE_H

#include <QObject>

class AppFolderFile : public QObject
{
    Q_OBJECT
public:
    explicit AppFolderFile(QObject *parent = nullptr);
    void serch_dir(QString AppDir);
signals:

};

#endif // APPFOLDERFILE_H
