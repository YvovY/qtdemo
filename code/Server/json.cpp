#include "json.h"
#include"config.h"
#include <QJsonDocument>

jSon::jSon(QObject *parent)
    : QObject{parent}
{

}

void jSon::writeJsonDocument(QFile& file,QJsonObject jsonObj)
{
    QJsonDocument jsonDocument;
    jsonDocument.setObject(jsonObj);
    QByteArray JsonData = jsonDocument.toJson();
    file.write(JsonData);
}

void jSon::writeJsonDocumentDefault(QFile& file)
{
    QJsonObject rootObj;
    QJsonValue val;
    rootObj.insert("tabWidgetIndex","0");
    tabWidgetIndex = QString::number(0);
    rootObj.insert("autoRun","0");
    autoRun = QString::number(0);
    rootObj.insert("radioButtonIndex","0");
    radioButtonIndex = QString::number(0);
    rootObj.insert("configFile",configDataUrl+"/config.json");
    configFile = configDataUrl+"/config.json";
    QJsonDocument jsonDocument;
    jsonDocument.setObject(rootObj);
    QByteArray JsonData = jsonDocument.toJson();
    file.write(JsonData);
}

void jSon::readJsonDocument(QByteArray JsonData)
{
    QJsonParseError error;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(JsonData,&error);
    if(error.error != QJsonParseError::NoError)
    {
        qDebug()<<"Json文件读取错误["<<error.errorString()<<"]";
    }
    if(jsonDocument.isObject())
    {
        QJsonObject rootObj = jsonDocument.object();
        QJsonObject::Iterator objIt;
        for(objIt = rootObj.begin(); objIt != rootObj.end(); objIt++)
        {
            qDebug()<<objIt.key()<<":"<<objIt.value();
            int i = switchMwnu(objIt.key());
            switch (i) {
            case 0:
                autoRun = objIt.value().toString();
                break;
            case 1:
                configFile = objIt.value().toString();
                break;
            case 2:
                radioButtonIndex = objIt.value().toString();
                break;
            case 3:
                tabWidgetIndex = objIt.value().toString();
                break;
            }
        }
    }
}

int jSon::switchMwnu(QString jsonKey)
{
    QStringList list;
    list<<"autoRun"<<"configFile"<<"radioButtonIndex"<<"tabWidgetIndex";
    for(int i=0; i < list.size(); i++)
    {
        if(list[i] == jsonKey)
        {
            return i;
        }
    }
    return -1;
}
