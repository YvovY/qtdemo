#include"config.h"
#include "widget.h"
#include <QApplication>
#include <QFile>
#include"AppFolderFile.h"
#include"json.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AppFolderFile aff;
    aff.serch_dir(QApplication::applicationDirPath());
    QString ConfigFile = configDataUrl+"/config.json";
    QFile file(ConfigFile);
    bool ok = file.exists();
    jSon js;
    if(ok){
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray JsonData = file.readAll();
        js.readJsonDocument(JsonData);
    }else
    {
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        js.writeJsonDocumentDefault(file);
    }
    file.close();
    Widget w;
    if(radioButtonIndex != "0")
    {
        w.show();
    }else
    {
        w.setVisible(false);
    }
    return a.exec();
}
