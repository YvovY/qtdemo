#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include<QSqlDatabase>
#include <QSqlRecord>
class DataBase: public QObject
{
    Q_OBJECT
private:
    QString dbName = "DataBase.db3";
    QString tbName = "userdata";
    QSqlDatabase db;//数据库指针
    QSqlRecord record;//数据库记录结果
public:
    void initDatabase();//初始化
    QSqlDatabase getdb();
    bool querycode(QSqlQuery& q);
    void closedb();//关闭数据库
    QSqlRecord getrecord();
};
#endif // DATABASE_H
/**

  数据库

  数据库名：
        用户数据库【UserDataBase】
            表名：userdata
            字段：手机号[账号]  [phoneNumber_acountNumber] INTEGER
                 用户名       [userName] varchar
                 密码         [passWord] integer
                 密文         [ciphertext] varchar
                 在线状态      [onlineStatus] varchar
                 头像          [avatarPicture] varchar
                 头像图片地址   [avatarPictureUrl] varchar
             建表：
CREATE TABLE "main"."userdata" (
  "phoneNumber_acountNumber" INTEGER(11) NOT NULL,
  "userName" varchar NOT NULL,
  "passWord" integer NOT NULL,
  "ciphertext" varchar NOT NULL,
  "avatarPicture" varchar NOT NULL,
  "avatarPictureUrl" varchar NOT NULL,
  "onlineStatus" varchar NOT NULL,
  PRIMARY KEY ("phoneNumber_acountNumber")
)


        书籍数据库【BookDataBase】
            表明：xuptlibrary
            字段: 封面地址        [CoverImageUrl] varchar
                 书名           [BookName] varchar
                 作者           [Author] varchar
                 ISBN码         [ISBNcode] varchar
                 出版社          [BookConcern] varchar
                 楼层            [Floor] varchar
                 书库            [StorageRoom] varchar
                 书架            [Bookshelf] varchar
                 入库数量         [TotalBookNum] integer
                 库存数量         [CurrentQuantity] integer
                 简介             [Synopsis] varchar
            建表：
CREATE TABLE "main"."xuptlibrary" ("BookName" varchar NOT NULL,"ISBNcode" varchar NOT NULL,"CoverImageUrl" varchar NOT NULL,"Author" varchar NOT NULL,"BookConcern" varchar NOT NULL,"Floor" varchar NOT NULL,"StorageRoom" varchar NOT NULL,"Bookshelf" varchar NOT NULL,"TotalBookNum" integer NOT NULL,"CurrentQuantity" integer NOT NULL,"Synopsis" varchar NOT NULL,PRIMARY KEY ("BookName"))


 **/
