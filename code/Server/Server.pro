QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
CONFIG += resources_big
CONFIG(debug, debug|release){
    MOC_DIR = "$$OUT_PWD/tmp/debug/.moc"
    OBJECTS_DIR = "$$OUT_PWD/tmp/debug/.obj"
    UI_DIR = "$$OUT_PWD/tmp/debug/.ui"
    RCC_DIR = "$$OUT_PWD/tmp/debug/.qrc"
}
CONFIG(release, debug|release){
    MOC_DIR = "$$OUT_PWD/tmp/release/.moc"
    OBJECTS_DIR = "$$OUT_PWD/tmp/release/.obj"
    UI_DIR = "$$OUT_PWD/tmp/release/.ui"
    RCC_DIR = "$$OUT_PWD/tmp/release/.qrc"
}

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AppFolderFile.cpp \
    SC.cpp \
    addbook.cpp \
    bookinfo.cpp \
    config.cpp \
    database.cpp \
    deletebook.cpp \
    deleteuser.cpp \
    json.cpp \
    main.cpp \
    socketthread.cpp \
    userevent.cpp \
    userinfo.cpp \
    widget.cpp

HEADERS += \
    AppFolderFile.h \
    SC.h \
    addbook.h \
    bookinfo.h \
    config.h \
    database.h \
    deletebook.h \
    deleteuser.h \
    filetransfer.hpp \
    json.h \
    socketthread.h \
    userevent.h \
    userinfo.h \
    widget.h

FORMS += \
    addbook.ui \
    bookinfo.ui \
    deletebook.ui \
    deleteuser.ui \
    userinfo.ui \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    serverQrc.qrc

LIBS += -lws2_32

RC_ICONS = 13.ico
