#ifndef DELETEBOOK_H
#define DELETEBOOK_H

#include <QDialog>
#include <QListWidgetItem>
#include"database.h"
namespace Ui {
class deletebook;
}

class deletebook : public QDialog
{
    Q_OBJECT
private:
    Ui::deletebook *ui;
    QString bookname = NULL;
    QString picture = NULL;
    DataBase D;
public:
    explicit deletebook(QWidget *parent = nullptr);
    ~deletebook();
signals:
    void updatebook();
private slots:
    void on_pushButton_clicked();

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);


    void on_lineEdit_textChanged(const QString &arg1);
};

#endif // DELETEBOOK_H
