#ifndef DELETEUSER_H
#define DELETEUSER_H

#include <QDialog>
#include <QListWidgetItem>
#include <QVector>
#include"database.h"
namespace Ui {
class deleteuser;
}

class deleteuser : public QDialog
{
    Q_OBJECT
    DataBase D;
    QString text;
public:
    explicit deleteuser(QWidget *parent = nullptr);
    ~deleteuser();
signals:
    void updateuser();
private slots:
    void on_serch_clicked();

    void on_result_itemDoubleClicked(QListWidgetItem *item);

    void on_account_textChanged(const QString &arg1);

private:
    Ui::deleteuser *ui;
};

#endif // DELETEUSER_H
