#ifndef WIDGET_H
#define WIDGET_H
#include <QSqlTableModel>
#include <QSystemTrayIcon>
#include <QWidget>
#include"config.h"
#include<QTcpServer>
#include"json.h"
#include <QProcess>
#include "socketthread.h"
#include"database.h"
#include"addbook.h"
#include"bookinfo.h"
#include"deletebook.h"
#include<QSqlQuery>
#include"deleteuser.h"
#include"userinfo.h"
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

private:
    userinfo* uinfo;
    deleteuser* deleteUser;
    QSqlTableModel* s;
    deletebook* deleteBook;
    bookinfo* bookInfo;
    addbook* addBook;
    DataBase D;
    Ui::Widget *ui;//ui文件指针
    class socketThread* threads;//线程指针
    QSystemTrayIcon* trayIcon;//托盘图标
    QTcpServer* tcpServer;//服务器
public:
    Widget(QWidget *parent = nullptr);//构造函数
    ~Widget();//析构函数
    void initFunc();//一些初始化函数集合
    void connections();//connections集合
protected:
    void closeEvent(QCloseEvent *event);//关闭事件
private:
    void showBookDataBase();//展示书本数据库内容
    void showUserDataBase();//展示用户数据
    void showcollect();//展示收藏书本数据
    void Appstart();//程序启动时的一些初始化函数
    void autoRunCk();//开机自启动
    void CreateSystemTrayIcon();//添加系统托盘
    void server();//服务端启动函数
    void closeApp();//程序关闭时进行的操作
private slots:
    void clientSocket();//链接套接字
    void threadRecvMsg(QByteArray data);//读取数据
    void onHostAddr(QByteArray data);//客户端地址
    void on_tabWidget_tabBarClicked(int index);//标签窗口点击各个标签时进行的操作
    void on_addBook_clicked();//添加书本按钮被点击
    void on_alterBookInfo_clicked();//书本信息修改按钮
    void on_deleteBook_clicked();//删除书本按钮
    void on_pushButton_deleteuser_clicked();//删除用户
    void on_pushButton_userinfo_clicked();
};
#endif // WIDGET_H
