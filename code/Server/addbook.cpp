#include "addbook.h"
#include "ui_addbook.h"

#include <QFileDialog>
#include<QDebug>
#include <QPixmap>
#include <QMessageBox>
#include"config.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
addbook::addbook(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addbook)
{
    ui->setupUi(this);
    ui->picture->setText("选择图片");
}

addbook::~addbook()
{
    delete ui;
}

void addbook::on_picture_clicked()
{
    QString title = "选择图片";
    QString filter = "图片 (*.jpg *.jpeg *.png);;";
    QString openddir = QDir::homePath();
    QString fileDir = QFileDialog::getOpenFileName(this,title,openddir,filter);
    if(fileDir.isEmpty())
    {
        ui->picture->setIcon(QIcon(""));
        ui->picture->setText("选择图片");
        QMessageBox::information(this,"提示","请选择图片！");
        return;
    }
    pictureurl = fileDir;
    qDebug()<<fileDir;
    QPixmap icon(fileDir);
    ui->picture->setText("");
    ui->picture->setIconSize(QSize(ui->picture->width()-10,ui->picture->height()-10));
    icon = icon.scaled(ui->picture->width(), ui->picture->height());
    ui->picture->setIcon(icon);
}


void addbook::on_bookname_textChanged(const QString &arg1)
{
    bookname = arg1;
    qDebug()<<arg1;
}


void addbook::on_isbn_textChanged(const QString &arg1)
{
    isbncode = arg1;
    qDebug()<<arg1;
}


void addbook::on_bookroom_textChanged(const QString &arg1)
{
    bookroom = arg1;
    qDebug()<<arg1;
}


void addbook::on_author_textChanged(const QString &arg1)
{
    author = arg1;
    qDebug()<<arg1;
}


void addbook::on_bookshelf_textChanged(const QString &arg1)
{
    bookshelf = arg1;
    qDebug()<<arg1;
}


void addbook::on_publisher_textChanged(const QString &arg1)
{
    publisher = arg1;
    qDebug()<<arg1;
}


void addbook::on_totalnum_textChanged(const QString &arg1)
{
    totalnum = arg1;
    qDebug()<<arg1;
}


void addbook::on_floor_textChanged(const QString &arg1)
{
    floor = arg1;
    qDebug()<<arg1;
}


void addbook::on_currentnum_textChanged(const QString &arg1)
{
    counnum = arg1;
    qDebug()<<arg1;
}


void addbook::on_jianjie_textChanged()
{
    jianjie = ui->jianjie->toPlainText();
    qDebug()<<jianjie;
}


void addbook::on_btcel_clicked()
{
    ui->bookname->setText("");
    ui->isbn->setText("");
    ui->bookshelf->setText("");
    ui->bookroom->setText("");
    ui->author->setText("");
    ui->publisher->setText("");
    ui->totalnum->setText("");
    ui->floor->setText("");
    ui->Booktype->setText("");
    ui->currentnum->setText("");
    ui->jianjie->clear();
    ui->picture->setIcon(QIcon(""));
    ui->picture->setText("选择图片");
    this->close();
}


void addbook::on_btok_clicked()
{
    if(
            bookname == NULL || isbncode == NULL || bookshelf == NULL ||
            author == NULL || bookroom == NULL || publisher == NULL ||
            totalnum == NULL || counnum == NULL || floor == NULL ||
            jianjie == NULL || pictureurl == NULL || booktype == NULL
            )
    {
        QMessageBox::information(this,"提示","请填入还没填入的输入框！");
        return;
    }
    QFileInfo info(pictureurl);
    QString newUrl = bookpictureUrl+"/"+bookname+"frontCover."+info.suffix();
    qDebug()<<newUrl;
    bool ok = QFile::copy(pictureurl,newUrl);
    if(ok)
    {
        QFileInfo newpicture(newUrl);
        pictureurl = newpicture.fileName();
    }
    QSqlQuery query(D.getdb());
    query.prepare("INSERT INTO main.xuptlibrary(BookName,ISBNcode,CoverImage,Author,BookConcern,BookType,Floor,StorageRoom,Bookshelf,TotalBookNum,CurrentQuantity,Synopsis) VALUES (:BookName,:ISBNcode,:CoverImage,:Author,:BookConcern,:BookType,:Floor,:StorageRoom,:Bookshelf,:TotalBookNum,:CurrentQuantity,:Synopsis)");
    query.bindValue(":BookName",bookname.toUtf8());
    query.bindValue(":ISBNcode",isbncode.toUtf8());
    query.bindValue(":CoverImage",pictureurl.toUtf8());
    query.bindValue(":Author",author.toUtf8());
    query.bindValue(":BookConcern",publisher.toUtf8());
    query.bindValue(":BookType",booktype.toUtf8());
    query.bindValue(":Floor",floor.toUtf8());
    query.bindValue(":StorageRoom",bookroom.toUtf8());
    query.bindValue(":Bookshelf",bookshelf.toUtf8());
    query.bindValue(":TotalBookNum",totalnum.toInt());
    query.bindValue(":CurrentQuantity",counnum.toInt());
    query.bindValue(":Synopsis",jianjie.toUtf8());
    if(D.querycode(query))
    {
        QMessageBox::information(this,"提示","数据写入完毕！");
        ui->bookname->setText("");
        ui->isbn->setText("");
        ui->bookshelf->setText("");
        ui->bookroom->setText("");
        ui->author->setText("");
        ui->publisher->setText("");
        ui->totalnum->setText("");
        ui->floor->setText("");
        ui->currentnum->setText("");
        ui->jianjie->clear();
        ui->picture->setIcon(QIcon(""));
        ui->picture->setText("选择图片");
        ui->Booktype->clear();
        emit updatebook();
    }
}


void addbook::on_Booktype_textChanged(const QString &arg1)
{
    booktype = arg1;
    qDebug()<<arg1;
}

