#ifndef SOCKETTHREAD_H
#define SOCKETTHREAD_H


#include <QObject>
#include<QThread>
#include<QTcpSocket>
#include"database.h"
#include "SC.h"
#include"config.h"
#include <QEventLoop>
class socketThread : public QThread
{
    Q_OBJECT
private:
    filezip* fzip;
    DataBase D;
    QTcpSocket* socket;
    QString IPs;
    QString ports;
public:
    explicit socketThread(QObject *parent = nullptr);
    ~socketThread();
    void setSocket(QTcpSocket* socket);
protected:
    void run();
private:
    inline void task(const QByteArray& json);//任务
    inline int classnum(QString& classVal);
    inline void SignUpClass(QJsonDocument jsonDocument);
    inline void SignUp(QJsonDocument jsonDocument);
    inline void loadClass(QJsonObject obj);
    inline void masteruiclass(QJsonObject obj);
    inline void addCollectionbook(QJsonObject obj);
    inline void deleteCollectionbook(QJsonObject obj);
    inline void getdbfile(QJsonObject obj);
    inline void getuserpicture(QJsonObject obj);
    inline void getbookpicture(QJsonObject obj);
signals:
    void updateuser();
    void recvmsg(QByteArray data);
    void hostAddr(QByteArray data);
private slots:
    void readData();
    void hostDisconnected();
};

#endif // SOCKETTHREAD_H
