#include "widget.h"
#include "ui_widget.h"
#include<QCheckBox>
#include <QAction>
#include <QMenu>
#include <QSettings>
#include <QSystemTrayIcon>
#include<QDebug>
#include <QJsonObject>
#include <QSqlTableModel>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    trayIcon = new QSystemTrayIcon(this);
    tcpServer = new QTcpServer;
    ui->tabWidget->setCurrentIndex(1);
    initFunc();
    D.initDatabase();
}

Widget::~Widget()
{
    D.closedb();
    delete ui;
}

void Widget::initFunc()
{
    deleteBook = new deletebook;
    bookInfo = new bookinfo;
    addBook = new addbook;
    deleteUser = new deleteuser;
    uinfo = new userinfo;
    server();
    autoRunCk();
    connections();
    Appstart();
}

void Widget::connections()
{
    connect(ui->autorun,&QCheckBox::clicked,this,&Widget::autoRunCk);
    connect(tcpServer,&QTcpServer::newConnection,this,&Widget::clientSocket);
    connect(addBook,&addbook::updatebook,this,&Widget::showBookDataBase);
    connect(bookInfo,&bookinfo::updatebook,this,&Widget::showBookDataBase);
    connect(deleteBook,&deletebook::updatebook,this,&Widget::showBookDataBase);
    connect(deleteUser,&deleteuser::updateuser,this,&Widget::showUserDataBase);
    connect(uinfo,&userinfo::updateuser,this,&Widget::showUserDataBase);
}

void Widget::Appstart()
{
    if(ui->hided->isCheckable())
    {
        CreateSystemTrayIcon();
    }else
    {
        this->show();
    }
    if(autoRun == "0")
    {
        ui->autorun->setChecked(false);
    }else
    {
        ui->autorun->setChecked(true);
    }
    ui->tabWidget->setCurrentIndex(tabWidgetIndex.toInt());
    if(radioButtonIndex == "0")
    {
        ui->hided->setChecked(true);
    }else
    {
        ui->quited->setChecked(true);
    }
}
void Widget::autoRunCk()
{
    QString appName = QApplication::applicationName();//程序名称
    QString appPath = QApplication::applicationFilePath();// 程序路径
    appPath = appPath.replace("/","\\");
    //    HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run
    //    HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Run
    //    第一个为全局自动启动项，所有用户登陆后都自动启动。第二个为当前用户启动项、切换到其他用户就失效了
    QSettings *reg=new QSettings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",QSettings::NativeFormat);
    if(ui->autorun->isChecked () == true)
    {
        QString val = reg->value(appName).toString();// 如果此键不存在，则返回的是空字符串
        if(val != appPath)
        {
            ui->message->appendPlainText ("开机自动启动已开启");
            reg->setValue(appName,appPath);// 如果移除的话，reg->remove(applicationName);
        }
    }
    else
    {
        reg->remove(appName);
        ui->message->appendPlainText ("开机自动启动已关闭");
    }
    reg->deleteLater();
}

void Widget::closeEvent(QCloseEvent *event)
{
    if(ui->hided->isChecked ())
    {
        event->ignore ();
        this->setVisible (false);
        this->hide ();
        CreateSystemTrayIcon();   //创建托盘
    }
    if(ui->quited->isChecked ())
    {
        closeApp();
    }
}

void Widget::showBookDataBase()
{
    s = new QSqlTableModel(this,D.getdb());
    s->setTable("xuptlibrary");
    QStringList list;
    list<<"书名"<<"ISBN码"<<"封面"<<"作者"<<"出版社"<<"书籍类型"<<"楼层"<<"书库"<<"书架"<<"入库数量"<<"库存数量"<<"简介";
    for(int i= 0; i < list.size();i++)
    {
        s->setHeaderData(i,Qt::Horizontal,list[i]);
    }
    ui->tableView_bookData->setModel(s);
    if(s->select())
    {
        ui->tableView_bookData->show();
    }else
    {
        qDebug()<<"查询失败";
    }
}

void Widget::showUserDataBase()
{
    ui->databaseaddr->setText("数据库位置:"+databasefileUrl);
    QSqlTableModel* s = new QSqlTableModel(this,D.getdb());
    s->setTable("userdata");
    QStringList list;
    list<<"账号"<<"用户名"<<"密码"<<"密文"<<"头像";
    for(int i= 0; i < list.size();i++)
    {
        s->setHeaderData(i,Qt::Horizontal,list[i]);
    }
    ui->tableView_userData->setModel(s);
    if(s->select())
    {
        ui->tableView_userData->show();
    }else
    {
        qDebug()<<"查询失败";
    }
}

void Widget::showcollect()
{
    QSqlTableModel* s = new QSqlTableModel(this,D.getdb());
    s->setTable("booktableCollection");
    QStringList list;
    list<<"用户名"<<"书名";
    for(int i= 0; i < list.size();i++)
    {
        s->setHeaderData(i,Qt::Horizontal,list[i]);
    }
    ui->tableView_collect->setModel(s);
    if(s->select())
    {
        ui->tableView_collect->show();
    }else
    {
        qDebug()<<"查询失败";
    }
}

void Widget::CreateSystemTrayIcon()
{
    //初始化两个活动项
    QAction* actionShow = new QAction("显示",this);
    QAction* actionclose = new QAction("关闭",this);
    //项1的点击槽函数
    connect(actionShow, &QAction::triggered, this, [=]()
    {
        this->show();
    });
    //项2的点击槽函数
    connect(actionclose , &QAction::triggered, this, [=]()
    {
        closeApp();
    });

    //初始化菜单并添加项
    QMenu* trayMenu = new QMenu(this);         //菜单
    trayMenu->addAction(actionShow);
    trayMenu->addAction(actionclose);

    //创建一个系统托盘
    trayIcon->setIcon(QIcon(trayIconAddr));    //设置托盘图标
    trayIcon->setContextMenu(trayMenu);        //设置菜单
    trayIcon->show();
}

void Widget::server()
{
    tcpServer->listen(QHostAddress::Any,APPport);
    if(tcpServer->isListening())
    {
        ui->message->appendPlainText("已开启监听！");
    }
    else
    {
        ui->message->appendPlainText("无法开启监听！");
    }
}

void Widget::closeApp()
{
    jSon json;
    QFile file(configFile);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonObject obj;
    obj.insert("tabWidgetIndex",QString::number(ui->tabWidget->currentIndex()));
    if(ui->hided->isChecked())
    {
        obj.insert("radioButtonIndex","0");
    }
    if(ui->quited->isChecked()){
        obj.insert("radioButtonIndex","1");
    }
    obj.insert("configFile",configFile);
    if(ui->autorun->isChecked())
    {
        obj.insert("autoRun","1");
    }else
    {
        obj.insert("autoRun","0");
    }

    json.writeJsonDocument(file,obj);
    file.close();
    QApplication::exit(0);
}

void Widget::clientSocket()
{
    threads = new class socketThread;
    threads->setSocket(tcpServer->nextPendingConnection());
    threads->start();
    connect(threads,&socketThread::recvmsg,this,&Widget::threadRecvMsg);
    connect(threads,&socketThread::hostAddr,this,&Widget::onHostAddr);
    connect(threads,&socketThread::updateuser,this,&Widget::showUserDataBase);
}
void Widget::threadRecvMsg(QByteArray data)
{
    ui->message->appendPlainText(data);
}

void Widget::onHostAddr(QByteArray data)
{
    QString com = data.left(3);
    QString addr = data.right(data.size() - 3);
    int comm = com == "add" ? 1 : (com == "del" ? 2 : -1);
    switch (comm) {
    case 1:
    {
        ui->message->appendPlainText("用户"+addr+"上线了");
    }
        break;
    case 2:
    {
        ui->message->appendPlainText("用户"+addr+"下线了");
    }
        break;
    }
}


void Widget::on_tabWidget_tabBarClicked(int index)
{
    switch (index) {
    case 0:
        break;
    case 1:
        showBookDataBase();
        break;
    case 2:
        showUserDataBase();
        break;
    case 3: showcollect();
        break;
    }
}


void Widget::on_addBook_clicked()
{
    addBook->setModal(true);
    addBook->exec();
}


void Widget::on_alterBookInfo_clicked()
{
    bookInfo->setModal(true);
    bookInfo->exec();
}


void Widget::on_deleteBook_clicked()
{
    deleteBook->setModal(true);
    deleteBook->exec();
}


void Widget::on_pushButton_deleteuser_clicked()
{
    deleteUser->setModal(true);
    deleteUser->exec();
}


void Widget::on_pushButton_userinfo_clicked()
{
    uinfo->setModal(true);
    uinfo->exec();
}

