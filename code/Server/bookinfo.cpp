#include "bookinfo.h"
#include "ui_bookinfo.h"
#include<QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QMessageBox>
#include <QPixmap>
#include <QSqlError>
#include <QSqlQuery>
#include<QFileInfo>
#include"config.h"
bookinfo::bookinfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::bookinfo)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
}

bookinfo::~bookinfo()
{
    delete ui;
}

void bookinfo::settext()
{
    ui->bookname->setText(bookname);
    ui->author->setText(author);
    ui->bookroom->setText(bookroom);
    ui->bookshelf->setText(bookshelf);
    ui->BookType->setText(booktype);
    ui->currentnum->setText(counnum);
    ui->floor->setText(floor);
    ui->isbn->setText(isbncode);
    ui->jianjie->appendPlainText(jianjie);
    ui->publisher->setText(publisher);
    ui->totalnum->setText(totalnum);
    QFileInfo info;
    QString thepix = bookpictureUrl+"/"+pictures;
    if(info.exists(thepix))
    {
        QPixmap icon(thepix);
        ui->picture->setText("");
        ui->picture->setIconSize(QSize(ui->picture->width()-10,ui->picture->height()-10));
        icon = icon.scaled(ui->picture->width(), ui->picture->height());
        ui->picture->setIcon(icon);
    }else
    {
        ui->picture->setText("选择图片");
    }
}

void bookinfo::on_lineEdit_textChanged(const QString &arg1)
{
    bookname = arg1;
    qDebug()<<arg1;
}


void bookinfo::on_pushButton_clicked()
{
    ui->listWidget->clear();
    if(ui->lineEdit->text() == NULL)
    {
        QMessageBox::information(this,"提示","请填入输入框！");
        return;
    }
    QSqlQuery query(D.getdb());
    QString code = "%"+bookname+"%";
    query.prepare("SELECT * FROM main.xuptlibrary WHERE BookName LIKE :BookName");
    query.bindValue(":BookName",code.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        ui->lineEdit->clear();
        while(query.next())
        {
            QString name = query.value(0).toString();
            ui->listWidget->addItem(name);
        }
    }else
    {
        qDebug()<<"搜索失败！["<<query.lastError().text()<<"]";
    }
}


void bookinfo::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    bookname = item->text();
    QSqlQuery query(D.getdb());
    query.prepare("SELECT * FROM main.xuptlibrary WHERE BookName LIKE :BookName");
    query.bindValue(":BookName",bookname.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        while(query.next())
        {
            bookname = query.value(0).toString();
            booknamedefalt = query.value(0).toString();
            qDebug()<<bookname;
            qDebug()<<booknamedefalt;
            isbncode = query.value(1).toString();
            qDebug()<<isbncode;
            pictures = query.value(2).toString();
            qDebug()<<pictures;
            author = query.value(3).toString();
            qDebug()<<author;
            publisher = query.value(4).toString();
            qDebug()<<publisher;
            booktype = query.value(5).toString();
            qDebug()<<booktype;
            floor = query.value(6).toString();
            qDebug()<<floor;
            bookroom = query.value(7).toString();
            qDebug()<<bookroom;
            bookshelf = query.value(8).toString();
            qDebug()<<bookshelf;
            totalnum = query.value(9).toString();
            qDebug()<<totalnum;
            counnum = query.value(10).toString();
            qDebug()<<counnum;
            jianjie = query.value(11).toString();
            qDebug()<<jianjie;
        }
    }else
    {
        qDebug()<<"搜索失败！["<<query.lastError().text()<<"]";
    }
    ui->stackedWidget->setCurrentIndex(1);
    settext();
}


void bookinfo::on_bookname_textChanged(const QString &arg1)
{
    bookname = arg1;
}


void bookinfo::on_isbn_textChanged(const QString &arg1)
{
    isbncode = arg1;
}


void bookinfo::on_author_textChanged(const QString &arg1)
{
    author = arg1;
}


void bookinfo::on_publisher_textChanged(const QString &arg1)
{
    publisher = arg1;
}


void bookinfo::on_floor_textChanged(const QString &arg1)
{
    floor = arg1;
}


void bookinfo::on_bookroom_textChanged(const QString &arg1)
{
    bookroom = arg1;
}


void bookinfo::on_bookshelf_textChanged(const QString &arg1)
{
    bookshelf = arg1;
}


void bookinfo::on_totalnum_textChanged(const QString &arg1)
{
    totalnum = arg1;
}


void bookinfo::on_currentnum_textChanged(const QString &arg1)
{
    counnum = arg1;
}


void bookinfo::on_jianjie_textChanged()
{
    jianjie = ui->jianjie->toPlainText();
}


void bookinfo::on_picture_clicked()
{
    QString title = "选择图片";
    QString filter = "图片 (*.jpg *.jpeg *.png);;";
    QString openddir = QDir::homePath();
    QString fileDir = QFileDialog::getOpenFileName(this,title,openddir,filter);
    if(fileDir.isEmpty())
    {
        ui->picture->setIcon(QIcon(""));
        ui->picture->setText("选择图片");
        QMessageBox::information(this,"提示","请选择图片！");
        return;
    }
    pictureDirss = fileDir;
    qDebug()<<fileDir;
    QPixmap icon(fileDir);
    ui->picture->setText("");
    ui->picture->setIconSize(QSize(ui->picture->width()-10,ui->picture->height()-10));
    icon = icon.scaled(ui->picture->width(), ui->picture->height());
    ui->picture->setIcon(icon);

}


void bookinfo::on_btok_clicked()
{
    /*
     UPDATE main.xuptlibrary SET BookName = '100', ISBNcode = '100', CoverImageUrl = '100', Author = '100', BookConcern = '100', Floor = '100', StorageRoom = '100', Bookshelf = '100', TotalBookNum = 100, CurrentQuantity = 100, Synopsis = '100' WHERE rowid = 1;
    */
    if(
            bookname == NULL || isbncode == NULL || bookshelf == NULL ||
            author == NULL || bookroom == NULL || publisher == NULL ||
            totalnum == NULL || counnum == NULL || floor == NULL ||
            jianjie == NULL || pictures == NULL || booktype == NULL
            )
    {
        QMessageBox::information(this,"提示","请填入还没填入的输入框！");
        return;
    }
    if(pictureDirss != NULL)
    {
        QString oldpicture = bookpictureUrl+"/"+pictures;
        qDebug()<<oldpicture;
        QFileInfo newfileInfo(pictureDirss);
        QString newUrl = bookpictureUrl+"/"+bookname+"frontCover."+newfileInfo.suffix();
        qDebug()<<newUrl;
        bool ok = QFile::remove(oldpicture);
        if(ok)
        {
            qDebug()<<"删除了原来的图片";
        }
        ok = QFile::copy(pictureDirss,newUrl);
        if(ok)
        {
            QFileInfo newpicture(newUrl);
            pictures = newpicture.fileName();
        }
    }
    QSqlQuery query(D.getdb());
    query.prepare("UPDATE main.xuptlibrary SET BookName = :BookName, ISBNcode = :ISBNcode, CoverImage = :CoverImage, Author = :Author, BookConcern = :BookConcern, BookType = :BookType, Floor = :Floor, StorageRoom = :StorageRoom, Bookshelf = :Bookshelf, TotalBookNum = :TotalBookNum, CurrentQuantity = :CurrentQuantity, Synopsis = :Synopsis WHERE BookName = :BookNamedef");
    query.bindValue(":BookName",bookname.toUtf8());
    query.bindValue(":ISBNcode",isbncode.toUtf8());
    query.bindValue(":CoverImage",pictures.toUtf8());
    query.bindValue(":Author",author.toUtf8());
    query.bindValue(":BookConcern",publisher.toUtf8());
    query.bindValue(":BookType",booktype.toUtf8());
    query.bindValue(":Floor",floor.toUtf8());
    query.bindValue(":StorageRoom",bookroom.toUtf8());
    query.bindValue(":Bookshelf",bookshelf.toUtf8());
    query.bindValue(":TotalBookNum",totalnum.toInt());
    query.bindValue(":CurrentQuantity",counnum.toInt());
    query.bindValue(":Synopsis",jianjie.toUtf8());
    query.bindValue(":BookNamedef",booknamedefalt.toUtf8());
    if(D.querycode(query))
    {
        QMessageBox::information(this,"提示","数据更新完毕！");
        ui->stackedWidget->setCurrentIndex(0);
        ui->listWidget->clear();
        ui->bookname->clear();
        ui->author->clear();
        ui->bookroom->clear();
        ui->bookshelf->clear();
        ui->currentnum->clear();
        ui->floor->clear();
        ui->isbn->clear();
        ui->jianjie->clear();
        ui->picture->setText("选择图片");
        ui->picture->setIcon(QIcon(""));
        ui->publisher->clear();
        ui->totalnum->clear();
        emit updatebook();
    }else
    {
        QMessageBox::information(this,"提示","数据更新失败！["+query.lastError().text()+"]");
    }
}


void bookinfo::on_btcel_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->bookname->clear();
    ui->author->clear();
    ui->bookroom->clear();
    ui->bookshelf->clear();
    ui->currentnum->clear();
    ui->floor->clear();
    ui->isbn->clear();
    ui->jianjie->clear();
    ui->picture->setText("选择图片");
    ui->picture->setIcon(QIcon(""));
    ui->publisher->clear();
    ui->totalnum->clear();
}


void bookinfo::on_BookType_textChanged(const QString &arg1)
{
    booktype = arg1;
}

