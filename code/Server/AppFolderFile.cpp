#include "AppFolderFile.h"
#include<QDebug>
#include <QDir>
#include"config.h"

AppFolderFile::AppFolderFile(QObject *parent)
    : QObject{parent}
{
}

void AppFolderFile::serch_dir(QString AppDir)
{
    QDir dir;
    QStringList folderList;
    folderList<<"AppDataDir"<<"ConfigDir"<<"DataBaseDir"<<"PictureDir"<<"Temp"<<"avtarpictureDir"<<"bookpictureDir";
    bool ok = dir.cd(AppDir);
    if(ok)
    {
        qDebug()<<"cd到"<<AppDir;
        applicationUrl = AppDir;
    }else
    {
        qDebug()<<"无法cd到此目录";
        return;
    }
    ok = dir.exists(folderList[0]);
    if(ok)
    {
        appDataUrl = applicationUrl+"/"+folderList[0];
        qDebug()<<"此目录存在"<<appDataUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[0]);
        appDataUrl = applicationUrl+"/"+folderList[0];
        qDebug()<<appDataUrl;
    }
    ok = dir.cd(appDataUrl);
    if(ok)
    {
        qDebug()<<"cd到"<<appDataUrl;
    }else
    {
        qDebug()<<"无法cd到此目录";
    }
    ok = dir.exists(folderList[1]);
    if(ok)
    {
        configDataUrl = appDataUrl+"/"+folderList[1];
        qDebug()<<"此目录存在"<<configDataUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[1]);
        configDataUrl = appDataUrl+"/"+folderList[1];
        qDebug()<<configDataUrl;
    }
    ok = dir.exists(folderList[2]);
    if(ok)
    {
        databaseUrl = appDataUrl+"/"+folderList[2];
        qDebug()<<"此目录存在"<<databaseUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[2]);
        databaseUrl = appDataUrl+"/"+folderList[2];
        qDebug()<<databaseUrl;
    }
    ok = dir.exists(folderList[3]);
    if(ok)
    {
        pictureUrl = appDataUrl+"/"+folderList[3];
        qDebug()<<"此目录存在"<<pictureUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[3]);
        pictureUrl = appDataUrl+"/"+folderList[3];
        qDebug()<<pictureUrl;
    }
    ok = dir.exists(folderList[4]);
    if(ok)
    {
        TempUrl = appDataUrl+"/"+folderList[4];
        qDebug()<<"此目录存在"<<TempUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[4]);
        TempUrl = appDataUrl+"/"+folderList[4];
        qDebug()<<TempUrl;
    }
    ok = dir.cd(pictureUrl);
    if(ok)
    {
        qDebug()<<"cd到"<<pictureUrl;
    }else
    {
        qDebug()<<"无法cd到此目录";
        return;
    }
    ok = dir.exists(folderList[5]);
    if(ok)
    {
        avtarpictureUrl = pictureUrl+"/"+folderList[5];
        qDebug()<<"此目录存在"<<avtarpictureUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[5]);
        avtarpictureUrl = pictureUrl+"/"+folderList[5];
        qDebug()<<avtarpictureUrl;
    }
    ok = dir.exists(folderList[6]);
    if(ok)
    {
        bookpictureUrl = pictureUrl+"/"+folderList[6];
        qDebug()<<"此目录存在"<<bookpictureUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[6]);
        bookpictureUrl = pictureUrl+"/"+folderList[6];
        qDebug()<<bookpictureUrl;
    }
}
