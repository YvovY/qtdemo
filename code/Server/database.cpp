#include "database.h"
#include"config.h"
#include<QDebug>
#include <QFileInfo>
#include <QSqlQuery>
#include<QSqlError>
#include <QSqlError>
#include <QSqlQuery>
void DataBase::initDatabase()
{
    if(QSqlDatabase::contains("qt_sql_default_connection"))
      db = QSqlDatabase::database("qt_sql_default_connection");
    else
      db = QSqlDatabase::addDatabase("QSQLITE");
    //1 查询数据库文件是否存在
    QString Database = databaseUrl+"/"+dbName;
    qDebug()<<Database;
    QFileInfo fileInfo(Database);
    db.setDatabaseName(Database);
    // 1.1 如果存在打开数据库不用初始化 return
    if(fileInfo.exists())
    {
        databasefileUrl = Database;
        bool ok = db.open();
        if(!ok)
        {
            qDebug()<<db.lastError().text();
        }else
        {
            qDebug()<<"数据库存在已打开";
        }
    }
    // 1.2 如果不存在
    else{
        databasefileUrl = Database;
        qDebug()<<"用户数据库不存在已创建并打开";
        // 1.2.1 先创建数据库文件 打开
        db.open();
        // 1.2.2 然后创建表
        if(db.isOpen())
        {
            QSqlQuery query(db);//数据库事件
            bool ok = query.exec(createusertable);
            if(ok)
            {
                qDebug()<<"用户数据库表创建成功";
            }else{
                qDebug()<<"用户数据库表创建失败："<<query.lastError().text();
            }
            ok = query.exec(createbooktable);
            if(ok)
            {
                qDebug()<<"书本数据库表创建成功";
            }else{
                qDebug()<<"书本数据库表创建失败："<<query.lastError().text();
            }
            ok = query.exec(createbooktableCollection);
            if(ok)
            {
                qDebug()<<"书本收藏表创建成功";
            }else{
                qDebug()<<"书本收藏表创建失败："<<query.lastError().text();
            }
        }else
        {
            qDebug()<<db.lastError().text();
        }
    }
}

QSqlDatabase DataBase::getdb()
{
    return db;
}

bool DataBase::querycode(QSqlQuery &q)
{
    bool ok = q.exec();
    if(ok == false)
    {
        const QSqlError& error = q.lastError();
        qDebug()<<"query语句运行失败！["<<error.text()<<"]";
    }
    return ok;
}


void DataBase::closedb()
{
    if(db.isOpen())
    {
    db.close();
    }
}

QSqlRecord DataBase::getrecord()
{
    return record;
}
