#ifndef ADDBOOK_H
#define ADDBOOK_H

#include <QDialog>
#include"database.h"
namespace Ui {
class addbook;
}

class addbook : public QDialog
{
    Q_OBJECT
private:
    DataBase D;
    Ui::addbook *ui;//ui指针
    QString bookname = NULL;//书名
    QString isbncode = NULL;//isbn数
    QString bookshelf = NULL;//书架
    QString author = NULL;//作者
    QString bookroom = NULL;//书库
    QString publisher = NULL;//出版社
    QString totalnum = NULL;//总数量
    QString counnum = NULL;//现有数量
    QString floor = NULL;//楼层
    QString jianjie = NULL;//简介
    QString pictureurl = NULL;//图片地址
    QString booktype = NULL;//书籍类型
public:
    explicit addbook(QWidget *parent = nullptr);
    ~addbook();
signals:
    void updatebook();
private slots:
    void on_picture_clicked();
    void on_bookname_textChanged(const QString &arg1);
    void on_isbn_textChanged(const QString &arg1);
    void on_bookroom_textChanged(const QString &arg1);
    void on_author_textChanged(const QString &arg1);
    void on_bookshelf_textChanged(const QString &arg1);
    void on_publisher_textChanged(const QString &arg1);
    void on_totalnum_textChanged(const QString &arg1);
    void on_floor_textChanged(const QString &arg1);
    void on_currentnum_textChanged(const QString &arg1);
    void on_jianjie_textChanged();
    void on_btcel_clicked();
    void on_btok_clicked();
    void on_Booktype_textChanged(const QString &arg1);
};

#endif // ADDBOOK_H
