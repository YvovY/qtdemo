
#include "socketthread.h"
#include<QDebug>
#include<QHostAddress>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QSqlQuery>
#include<QSqlError>
#include <QDir>


socketThread::socketThread(QObject *parent): QThread{parent}
{
    socket = new QTcpSocket;
    fzip = new filezip;
}

socketThread::~socketThread()
{

}

void socketThread::setSocket(QTcpSocket *socket)
{
    this->socket = socket;
}

void socketThread::run()
{
    IPs = socket->peerAddress().toString ();
    IPs = IPs.remove(0,7);
    ports = QString::number(socket->peerPort());
    emit hostAddr("add"+IPs.toUtf8()+"/"+ports.toUtf8());
    connect(socket,&QTcpSocket::readyRead,this,&socketThread::readData);
    connect(socket,&QTcpSocket::disconnected,this,&socketThread::hostDisconnected);

}

void socketThread::task(const QByteArray& json)
{
    QJsonParseError error;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json,&error);
    if(error.error != QJsonParseError::NoError)
    {
        qDebug()<<"Json文件读取错误["<<error.errorString()<<"]";
    }
    QJsonObject obj = jsonDocument.object();
    QJsonObject::Iterator jsonIt;
    QString classVal =NULL;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "class")
        {
            classVal = jsonIt.value().toString();
        }
    }
    int switchNum = classnum(classVal);
    switch(switchNum)
    {
    case -1: break;
    case 0: SignUpClass(jsonDocument); break;
    case 1: loadClass(obj); break;
    case 2: masteruiclass(obj); break;
    case 3: break;
    case 4: break;
    case 5: break;
    case 6: break;
    case 7: break;
    case 8: break;
    case 9: break;
    case 10: break;
    case 11: break;
    case 12: break;
    }
}

int socketThread::classnum(QString& classVal)
{
    QStringList classList;
    classList<<"SignUp"<<"load"<<"MasterUI";
    for(int i = 0; i < classList.size(); i++)
    {
        if(classList[i].data() == classVal)
        {
            return i;
        }
    }
    return -1;
}

void socketThread::SignUpClass(QJsonDocument jsonDocument)
{
    int switchNum = -1;
    QString comm;
    QStringList commandlists;
    commandlists<<"SinNewUser";
    if(jsonDocument.isObject())
    {
        QJsonObject obj = jsonDocument.object();
        QJsonObject::Iterator jsonIt;
        for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
        {
            if(jsonIt.key() == "command")
            {
                comm = jsonIt.value().toString();
            }
        }
    }
    for(int i=0; i< commandlists.size(); i++)
    {
        if(commandlists[i].data() == comm)
        {
            switchNum = i;
        }
    }
    switch(switchNum)
    {
    case -1: break;
    case 0: SignUp(jsonDocument); break;
    case 1: break;
    case 2: break;
    case 3: break;
    case 4: break;
    case 5: break;
    case 6: break;
    case 7: break;
    case 8: break;
    case 9: break;
    case 10: break;
    case 11: break;
    case 12: break;
    }
}

void socketThread::SignUp(QJsonDocument jsonDocument)
{
    //新用户注册
    QString phonenumber;
    QString username;
    QString passwd;
    QString miwen;
    QString avpicture;

    if(jsonDocument.isObject())
    {
        QJsonObject obj_comm = jsonDocument.object();
        for(auto Jit = obj_comm.begin(); Jit != obj_comm.end(); Jit++)
        {
            qDebug()<<"SignUp函数"<<Jit.key()<<":"<<Jit.value().toString();
            if(Jit.key() == "avpicture")
            {
                avpicture = Jit.value().toString();
            }
            if(Jit.key() == "miwen")
            {
                miwen = Jit.value().toString();
            }
            if(Jit.key() == "passwd")
            {
                passwd = Jit.value().toString();
            }
            if(Jit.key() == "phonenum")
            {
                phonenumber = Jit.value().toString();
            }
            if(Jit.key() == "username")
            {
                username = Jit.value().toString();
            }
        }
    }
    qDebug()<<avpicture<<" "<<miwen<<" "<<passwd<<" "<<phonenumber<<" "<<username;
    //1 检查此账号是否存在 SELECT phoneNumber_acountNumber FROM main.userdata WHERE phoneNumber_acountNumber = 0;
    QSqlQuery query(D.getdb());
    query.prepare("SELECT phoneNumber_acountNumber FROM main.userdata WHERE phoneNumber_acountNumber = :acountNumber");
    query.bindValue(":acountNumber",phonenumber.toUtf8());
    bool ok = query.exec();
    QString ret;
    if(ok)
    {
        while(query.next())
        {
            ret = query.value(0).toString();
        }
        qDebug()<<"检查此账号是否存在 "<<ret;
    }
    if(ret.isEmpty())//账号不存在
    {
        //2 把信息写入到数据库
        query.prepare("INSERT INTO main.userdata (phoneNumber_acountNumber,userName,passWord,ciphertext,avatarPicture) VALUES (:phoneNumber_acountNumber,:userName,:passWord,:ciphertext,:avatarPicture)");
        query.bindValue(":phoneNumber_acountNumber",phonenumber.toUtf8());
        query.bindValue(":userName",username.toUtf8());
        query.bindValue(":passWord",passwd.toUtf8());
        query.bindValue(":ciphertext",miwen.toUtf8());
        query.bindValue(":avatarPicture",avpicture.toUtf8());
        ok = query.exec();
        if(!ok)
        {
            qDebug()<<"querry运行失败！["<<query.lastError().text()<<"]";
        }
        else
        {
            emit updateuser();
            QString avdir = avtarpictureUrl+"/";
            Server2* SR = new Server2;
            SR->setFileSaveAddr(avdir);
            unsigned short port = SR->getPort();
            qDebug()<<"port:"<<port<<" dir:"<<avdir;
            QJsonObject objs;
            objs["class"] = "SignUp";
            objs["command"] = "successAddNewUser";
            objs["port"] = port;
            QJsonDocument doc;
            doc.setObject(objs);
            QByteArray data = doc.toJson();
            emit recvmsg(data);
            int rets = socket->write(data);
            if(rets != -1)
            {
                qDebug()<<"发送成功！";
                QEventLoop* el = new QEventLoop();
                connect(SR, &Server2::disconnects, el, &QEventLoop::quit);
                el->exec();
            }
        }
    }
    else
    {
        QJsonObject objs;
        objs["class"] = "SignUp";
        objs["command"] = "exists";
        QJsonDocument doc;
        doc.setObject(objs);
        QByteArray data = doc.toJson();
        emit recvmsg(data);
        int rets = socket->write(data);
        if(rets != -1)
        {
            qDebug()<<"发送成功！";
        }
    }
}

void socketThread::loadClass(QJsonObject obj)
{
    int switchNum = -1;
    QString comm;
    QStringList commandlists;
    commandlists<<"getdbfile"<<"getbookpicture"<<"getuserpicture";
    QJsonObject::Iterator jsonIt;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "command")
        {
            comm = jsonIt.value().toString();
        }
    }
    for(int i=0; i< commandlists.size(); i++)
    {
        if(commandlists[i].data() == comm)
        {
            switchNum = i;
        }
    }
    switch(switchNum)
    {
    case -1: break;
    case 0: getdbfile(obj); break;
    case 1: getbookpicture(obj); break;
    case 2: getuserpicture(obj); break;
    case 3: break;
    case 4: break;
    case 5: break;
    case 6: break;
    case 7: break;
    case 8: break;
    case 9: break;
    case 10: break;
    case 11: break;
    case 12: break;
    }
}

void socketThread::masteruiclass(QJsonObject obj)
{
    int switchNum = -1;
    QString comm;
    QStringList commandlists;
    commandlists<<"Collectionnewbook"<<"DeleteCollectionBook";
    QJsonObject::Iterator jsonIt;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "command")
        {
            comm = jsonIt.value().toString();
        }
    }
    for(int i=0; i< commandlists.size(); i++)
    {
        if(commandlists[i].data() == comm)
        {
            switchNum = i;
        }
    }
    switch(switchNum)
    {
    case -1:break;
    case 0: addCollectionbook(obj); break;
    case 1: deleteCollectionbook(obj); break;
    case 2: break;
    case 3: break;
    case 4: break;
    case 5: break;
    case 6: break;
    case 7: break;
    case 8: break;
    case 9: break;
    case 10: break;
    case 11: break;
    case 12: break;
    }
}

void socketThread::addCollectionbook(QJsonObject obj)
{
    qDebug()<<"书本收藏表";
    QJsonObject::Iterator jsonIt;
    QString text = NULL;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "text")
        {
            text = jsonIt.value().toString();
        }
    }
    QString username, bookname;
    username = text.section("#",0,0);
    qDebug()<<"username: "<<username;
    bookname = text.section("#",1,1);
    qDebug()<<"bookname: "<<bookname;
    QSqlQuery query(D.getdb());
    query.prepare("SELECT UserName FROM main.booktableCollection WHERE BookName = :BookName");
    query.bindValue(":BookName",bookname.toUtf8());
    query.exec();
    QStringList valList;
    while (query.next()) {
        valList << query.value(0).toString();
    }
    bool flags = true;
    for(int i = 0; i < valList.size(); i++)
    {
        if(valList[i] == username)
        {
            flags = false;
        }
    }
    if(flags)
    {
        qDebug()<<"此纪录不存在加入进去了";
        query.prepare("INSERT INTO main.booktableCollection (UserName,BookName) VALUES(:UserName,:BookName)");
        query.bindValue(":UserName",username.toUtf8());
        query.bindValue(":BookName",bookname.toUtf8());
        bool ok = query.exec();
        if(!ok)
        {
            qDebug()<<"querry运行失败！["<<query.lastError().text()<<"]";
        }
    }
    else
    {
        qDebug()<<"此纪录存在不加入进去了";
    }
}

void socketThread::deleteCollectionbook(QJsonObject obj)
{
    qDebug()<<"书本收藏表";
    QJsonObject::Iterator jsonIt;
    QString text = NULL;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "text")
        {
            text = jsonIt.value().toString();
        }
    }
    QString username, bookname;
    username = text.section("##",0,0);
    qDebug()<<"username: "<<username;
    bookname = text.section("##",1,1);
    bookname = bookname.remove(0,3);
    qDebug()<<"bookname: "<<bookname;
    QSqlQuery query(D.getdb());
    query.prepare("DELETE FROM main.booktableCollection WHERE BookName=:BookName AND UserName=:UserName");
    query.bindValue(":BookName",bookname.toUtf8());
    query.bindValue(":UserName",username.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        qDebug()<<"删除了一条收藏记录:"<<bookname<<" "<<username;
    }
    else
    {
        qDebug()<<"没能删除此纪录: "<<query.lastError().text();
    }
}

void socketThread::getdbfile(QJsonObject obj)
{
    QJsonObject::Iterator jsonIt;
    int Val =NULL;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "port")
        {
            Val = jsonIt.value().toInt();
        }
    }
    Client2* send = new Client2;
    send->setPort(Val);
    send->setFilePath(databaseUrl+"/"+"DataBase.db3");
    send->connectToServer(IP);
    QEventLoop* el = new QEventLoop();
    connect(send, &Client2::disconnects, el, &QEventLoop::quit);
    el->exec();
}

void socketThread::getuserpicture(QJsonObject obj)
{
    QJsonObject::Iterator jsonIt;
    int Val =NULL;
    QString userpicture =NULL;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "port")
        {
            Val = jsonIt.value().toInt();
        }
        if(jsonIt.key() == "userpicture")
        {
            userpicture = jsonIt.value().toString();
        }
    }
    Client2* send = new Client2;
    send->setPort(Val);
    send->setFilePath(avtarpictureUrl+"/"+userpicture);
    send->connectToServer(IP);
    QEventLoop* el = new QEventLoop();
    connect(send, &Client2::disconnects, el, &QEventLoop::quit);
    el->exec();
}

void socketThread::getbookpicture(QJsonObject obj)
{
    //随机生成6为字符串
    qsrand(QDateTime::currentMSecsSinceEpoch());
    const char ch[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int size = sizeof(ch);

    char* str = new char[6 + 1];

    int num = 0;
    for (int i = 0; i < 6; ++i)
    {
        num = rand() % (size - 1);
        str[i] = ch[num];
    }
    QString res(str);
    //压缩书本图片
    qDebug()<<"压缩书本图片";
    QString savepath = QDir::currentPath() + "/"+ res + ".zip";
    QString inputpath = bookpictureUrl + "/*";
    fzip->zip(savepath,inputpath);
    //发送图片
    qDebug()<<"发送图片";
    QJsonObject::Iterator jsonIt;
    int Val =NULL;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "port")
        {
            Val = jsonIt.value().toInt();
        }
    }
    Client2* send = new Client2;
    send->setPort(Val);
    send->setFilePath(savepath);
    send->connectToServer(IP);
    QEventLoop* el = new QEventLoop();
    connect(send, &Client2::disconnects, el, &QEventLoop::quit);
    el->exec();
    //删除图片
    qDebug()<<"删除图片";
    QFile::remove(savepath);
}
void socketThread::readData()
{
    QByteArray json = socket->readAll();
    emit recvmsg(json);
    task(json);
}

void socketThread::hostDisconnected()
{
    if(this->isRunning())
    {
        this->quit();
    }
    emit hostAddr("del"+IPs.toUtf8()+"/"+ports.toUtf8());
}
