#include "userinfo.h"
#include "qdebug.h"
#include "qsqlerror.h"
#include "ui_userinfo.h"

#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QSqlQuery>

userinfo::userinfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::userinfo)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
}

userinfo::~userinfo()
{
    delete ui;
}

void userinfo::on_account_textEdited(const QString &arg1)
{
    text = arg1;
}


void userinfo::on_serch_clicked()
{
    ui->result->clear();
    if(ui->account->text() == NULL)
    {
        QMessageBox::information(this,"提示","请填入输入框！");
        return;
    }
    QSqlQuery query(D.getdb());
    QString code = "%"+text+"%";
    query.prepare("SELECT * FROM main.userdata WHERE phoneNumber_acountNumber LIKE :phoneNumber_acountNumber");
    query.bindValue(":phoneNumber_acountNumber",code.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        ui->account->clear();
        while(query.next())
        {
            QString account = query.value(0).toString();
            QString username = query.value(1).toString();
            QString avpicture = query.value(4).toString();
            ui->result->addItem(account+":[用户名:"+username+"][头像:"+avpicture+"]");
        }
    }else
    {
        qDebug()<<"搜索失败！["<<query.lastError().text()<<"]";
    }
}


void userinfo::on_result_itemDoubleClicked(QListWidgetItem *item)
{
    QString account = item->text().section(":",0,0);
    QSqlQuery query(D.getdb());
    QString code = "%"+account+"%";
    query.prepare("SELECT * FROM main.userdata WHERE phoneNumber_acountNumber LIKE :phoneNumber_acountNumber");
    query.bindValue(":phoneNumber_acountNumber",code.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        ui->account->clear();
        while(query.next())
        {
            accountsss = query.value(0).toString();
            usernamesss = query.value(1).toString();
            passwdsss = query.value(2).toString();
            miwensss = query.value(3).toString();
            avpicturesss = query.value(4).toString();
        }
    }else
    {
        qDebug()<<"搜索失败！["<<query.lastError().text()<<"]";
    }
    ui->accounts->setText(accountsss);
    ui->usernames->setText(usernamesss);
    ui->passwd->setText(passwdsss);
    ui->miwen->setText(miwensss);
    ui->stackedWidget->setCurrentIndex(1);
    ui->result->clear();
}


void userinfo::on_reset_clicked()
{
    QString resetuseraccount = ui->accounts->text();
    QString resetusername = ui->usernames->text();
    QString resetuserpasswd = ui->passwd->text();
    QString resetusermiwen = ui->miwen->text();
    QString olddir = avtarpictureUrl+"/"+avpicturesss;
    QFileInfo info(olddir);
    QString filesuffix = info.suffix();
    QString resetavpicture = resetuseraccount+"User."+filesuffix;
    QSqlQuery query(D.getdb());
    query.prepare("UPDATE main.userdata SET phoneNumber_acountNumber = :phoneNumber_acountNumber,userName = :userName,passWord = :passWord,ciphertext = :ciphertext,avatarPicture = :avatarPicture WHERE phoneNumber_acountNumber LIKE :phoneNumber");
    query.bindValue(":phoneNumber_acountNumber",resetuseraccount.toUtf8());
    query.bindValue(":userName",resetusername.toUtf8());
    query.bindValue(":passWord",resetuserpasswd.toUtf8());
    query.bindValue(":ciphertext",resetusermiwen.toUtf8());
    query.bindValue(":avatarPicture",resetavpicture.toUtf8());
    query.bindValue(":phoneNumber",accountsss.toUtf8());
    if(D.querycode(query))
    {
        QMessageBox::information(this,"提示","数据写入完毕！");
        ui->accounts->setText("");
        ui->usernames->setText("");
        ui->passwd->setText("");
        ui->miwen->setText("");
        emit updateuser();
    }
    QDir dir;
    QString newdir = avtarpictureUrl+"/"+resetavpicture;
    dir.rename(olddir,newdir);
    ui->stackedWidget->setCurrentIndex(0);
}

