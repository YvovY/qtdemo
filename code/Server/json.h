#ifndef JSON_H
#define JSON_H

#include <QFile>
#include <QJsonObject>
#include <QObject>

class jSon : public QObject
{
    Q_OBJECT
public:
    explicit jSon(QObject *parent = nullptr);
    void writeJsonDocument(QFile& file,QJsonObject jsonObj);
    void writeJsonDocumentDefault(QFile& file);
    void readJsonDocument(QByteArray JsonData);
    int switchMwnu(QString jsonKey);
signals:

};

#endif // JSON_H
