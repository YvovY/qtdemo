#include "deleteuser.h"
#include "qdebug.h"
#include "qsqlerror.h"
#include "ui_deleteuser.h"
#include"config.h"
#include <QMessageBox>
#include <QSqlQuery>
#include<QFile>
deleteuser::deleteuser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::deleteuser)
{
    ui->setupUi(this);
}

deleteuser::~deleteuser()
{
    delete ui;
}

void deleteuser::on_serch_clicked()
{
    ui->result->clear();
    if(ui->account->text() == NULL)
    {
        QMessageBox::information(this,"提示","请填入输入框！");
        return;
    }
    QSqlQuery query(D.getdb());
    QString code = "%"+text+"%";
    query.prepare("SELECT * FROM main.userdata WHERE phoneNumber_acountNumber LIKE :phoneNumber_acountNumber");
    query.bindValue(":phoneNumber_acountNumber",code.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        ui->account->clear();
        while(query.next())
        {
            QString account = query.value(0).toString();
            QString username = query.value(1).toString();
            QString passwd = query.value(1).toString();
            QString miwen = query.value(1).toString();
            QString avpicture = query.value(4).toString();
            ui->result->addItem(account+":[用户名:"+username+"][头像:"+avpicture+"]");
        }
    }else
    {
        qDebug()<<"搜索失败！["<<query.lastError().text()<<"]";
    }
}


void deleteuser::on_result_itemDoubleClicked(QListWidgetItem *item)
{
    QString account = item->text().section(":",0,0);
    qDebug()<<account;
    QMessageBox::StandardButton bt = QMessageBox::question(this,"删除","确认删除此数据吗？",QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    if(bt == QMessageBox::Yes)
    {
        QSqlQuery query(D.getdb());
        query.prepare("DELETE FROM main.userdata WHERE phoneNumber_acountNumber LIKE :phoneNumber_acountNumber");
        query.bindValue(":phoneNumber_acountNumber",account.toUtf8());
        bool ok = query.exec();
        if(ok)
        {
            qDebug()<<"成功删除数据！";
            ui->result->clear();
            emit updateuser();
        }else
        {
            qDebug()<<"数据删除失败！["<<query.lastError().text()<<"]";
        }
        QString p = pictureUrl+"/"+"avpicture";
        qDebug()<<p;
        if(QFile::remove(p))
        {
            qDebug()<<"成功删除封面！";
        }
    }
    if(bt == QMessageBox::Yes)
    {
        ui->result->clear();
    }

}


void deleteuser::on_account_textChanged(const QString &arg1)
{
    text = arg1;
}

