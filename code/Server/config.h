#ifndef CONFIG_H
#define CONFIG_H

#include<QString>

extern QString trayIconAddr;//托盘图标
extern QString applicationUrl;//程序文件目录
extern QString appDataUrl;//程序数据目录
extern QString configDataUrl;//配置文件目录
extern QString databaseUrl;//数据库文件目录
extern QString pictureUrl;//图片文件目录
extern QString avtarpictureUrl;//头像图片文件目录
extern QString bookpictureUrl;//书本图片文件目录
extern QString tabWidgetIndex;//标签窗口索引
extern QString autoRun;//开机启动状态
extern QString radioButtonIndex;//单选按钮索引
extern QString configFile;//配置文件
extern QString databasefileUrl;//数据库文件
extern QString createusertable;//创建用户表
extern QString createbooktable;//创建书籍表
extern QString createbooktableCollection;//创建收藏表
extern QString TempUrl;//Temp文件地址



extern QString IP;
#define APPport 9999 //程序链接端口

#endif // CONFIG_H
