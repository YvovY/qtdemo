#ifndef USERINFO_H
#define USERINFO_H

#include <QDialog>
#include <QListWidgetItem>
#include"database.h"
#include"config.h"
namespace Ui {
class userinfo;
}

class userinfo : public QDialog
{
    Q_OBJECT
    QString text;
    QString accountsss;
    QString usernamesss;
    QString passwdsss;
    QString avpicturesss;
    QString miwensss;
    DataBase D;
public:
    explicit userinfo(QWidget *parent = nullptr);
    ~userinfo();
signals:
    void updateuser();
private slots:
    void on_account_textEdited(const QString &arg1);

    void on_serch_clicked();

    void on_result_itemDoubleClicked(QListWidgetItem *item);

    void on_reset_clicked();

private:
    Ui::userinfo *ui;
};

#endif // USERINFO_H
