#ifndef FORGOTPASSWORD_H
#define FORGOTPASSWORD_H

#include <QWidget>
#include<QDialog>
namespace Ui {
class ForgotPassword;
}

class ForgotPassword : public QDialog
{
    Q_OBJECT

public:
    explicit ForgotPassword(QWidget *parent = nullptr);
    ~ForgotPassword();

private slots:
    void on_AccountNumber_toggled(bool checked);

    void on_VerificationCode_toggled(bool checked);

    void on_newPasswd_toggled(bool checked);

private:
    Ui::ForgotPassword *ui;
};

#endif // FORGOTPASSWORD_H
