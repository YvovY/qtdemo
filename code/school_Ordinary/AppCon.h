#ifndef APPCON_H
#define APPCON_H

#include <QObject>

class AppCon : public QObject
{
    Q_OBJECT
public:
    explicit AppCon(QObject *parent = nullptr);
    void serch_dir(QString AppDir);

};

#endif // APPCON_H
