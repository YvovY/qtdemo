#include "SC.h"

#include <QFileInfo>
#include <QProcess>

Server::Server(QObject *parent)
    : QObject{parent}
{
    ss = new QTcpServer;
    so = new QTcpSocket;
    sotimer = new QTimer;
    connect(sotimer,&QTimer::timeout,this,[=](){
        sotimer->stop();
        sendData();
    });
}

bool Server::startlistern()
{
    emit message("开始监听");
    bool ok = ss->listen();
    if(ok)
    {
        this->port = ss->serverPort();
        emit getserverport(ss->serverPort());
        emit message("监听成功");
        connect(ss,&QTcpServer::newConnection,this,&Server::on_connection);
        return ok;
    }
    else
    {
        emit message("监听失败");
        return ok;
    }
}

void Server::stoplistern()
{
    emit message("服务器关闭");
    ss->close();
}

void Server::fileinfo(QString filepath)
{
    emit message("文件地址:"+filepath);
    QFileInfo fileinfo(filepath);
    filename = fileinfo.fileName();
    emit message("文件名:"+filename);
    filesize = fileinfo.size();
    emit message("文件大小:"+QString::number(filesize)+"B");
    sendsize = 0;
    file.setFileName(filepath);
    bool ok = file.open(QIODevice::ReadOnly);
    if(ok ==false)
    {
        emit message("文件未能打开 "+ QString::number(__LINE__));
    }
}

void Server::sendfile()
{
    //先发送文件信息
    QString str = QString("%1##%2").arg(filename).arg(filesize);//可在客户端解包
    QByteArray data = str.toUtf8();
    char* dat = data.data();
    qint64 len = so->write(dat);
    if(len > 0)//发送文件信息成功
    {
        //        发送真正的文件
        //        距离太近会出现粘包问题
        //        用定时器解决粘包问题
        sotimer->start(20);
    }
    else
    {
        emit message("文件信息发送失败 "+ QString::number(__LINE__));
        file.close();
        so->disconnectFromHost();
        so->close();
    }
}

void Server::sendData()
{
    qint64 len = 0;
    do{
        char buf[4*1024]{0};
        //读文件内容
        len = file.read(buf,sizeof(buf));//读了多少文件内容
        //读多少发多少
        len = so->write(buf,len);//返回已经发送多少

        sendsize += len;//已经发送多少内容
        emit processnum((sendsize / filesize) * 100);
    }while(len > 0);
}

int Server::getPort()
{
    return this->port;
}

void Server::on_connection()
{
    so = ss->nextPendingConnection();
    connect(so,&QTcpSocket::readyRead,this,[=](){
        QByteArray array = so->readAll();
        if(array == "RecvDone")
        {
            emit message("文件已全部发送完毕");
            file.close();
            so->disconnectFromHost();
            so->close();
        }
    });
    QString ip = so->peerAddress().toString();
    int port = so->peerPort();
    QString text = ip +" "+ QString::number(port);
    emit message(text);

}

Client::Client(QObject *parent)
    : QObject{parent}
{
    co = new QTcpSocket;
    isStart = true;
}

void Client::connectToServer(QString IP, int port)
{
    emit message("开始链接");
    co->connectToHost(QHostAddress(IP),port);
    connect(co,&QTcpSocket::connected,this,[=](){
        emit message("连接到客户端");
    });
    connect(co,&QTcpSocket::readyRead,this,[=](){
        QByteArray array = co->readAll();
        if(isStart == true)
        {
            isStart = false;
            //接受文件并解包
            //hello##1024 拆包 section
            filename = QString(array).section("##",0,0);
            emit message("文件名:"+filename);
            filesize = QString(array).section("##",1,1).toLongLong();
            emit message("文件大小:"+QString::number(filesize));
            recvsize = 0;

            //创建文件设置为只写
            file.setFileName(filename);
            bool ok = file.open(QIODevice::WriteOnly);
            if(ok == false)
            {
                emit message("文件打开失败");
                co->disconnectFromHost();
                co->close();
                file.close();
            }
        }
        else//发送过来的是文件内容
        {
            qint64 len = file.write(array);
            recvsize += len;
            emit processnum((recvsize / filesize) * 100);
            emit message("文件接受大小:"+QString::number(recvsize));
            if(recvsize == filesize)
            {
                emit message("文件全部接受完毕");
                co->write("RecvDone");
                co->disconnectFromHost();
                co->close();
                file.close();
            }
        }
    });
}

filezip::filezip(QObject *parent): QObject{parent}{}

void filezip::zip(QString savepath, QString inputpath)
{
    QProcess* p = new QProcess;
    QString program = "7z.exe";
    QStringList arg;
    arg<<"a";
    arg<<"-y";
    arg<<"-tzip";
    arg<<savepath;
    arg<<inputpath;
    qDebug()<<arg;
    p->start(program,arg);
    p->waitForFinished();
    emit complete();
}

void filezip::unzip(QString outputpath, QString zippath)
{
    QProcess* p = new QProcess;
    QString program = "7z.exe";
    QStringList args;
    args<<"x";
    args<<"-y";
    args<<"-aos";
    QString str = "-o";
    str += outputpath;
    args<<str;
    args<<zippath;
    qDebug()<<args;
    p->start(program,args);
    p->waitForFinished();
    qDebug()<<"解压缩完毕";
    emit complete();
}

Server2::Server2(QObject *parent)
    : QObject{parent}
{
    ss = new QTcpServer;
    ss->listen();
    if(ss->isListening())
    {
        port = ss->serverPort();
        emit message("监听成功");
    }
    so = new QTcpSocket;
    connect(ss,&QTcpServer::newConnection,this,&Server2::on_newConnection);
}

void Server2::setFileSaveAddr(QString fileSavePath)
{
    this->fileSavePath = fileSavePath;
}

int Server2::getPort()
{
    return port;
}

void Server2::on_newConnection()
{
    so = ss->nextPendingConnection();
    emit message("连接到了客户端");
    connect(so,&QTcpSocket::readyRead,this,&Server2::on_readClientData);
    connect(so,&QTcpSocket::disconnected,this,[=](){
        emit message("断开了客户端");
        emit disconnects();
    });
}

void Server2::on_readClientData()
{
    QByteArray array = so->readAll();
    if(isStart == true)
    {
        isStart = false;
        //接受文件并解包
        //hello##1024 拆包 section
        filename = QString(array).section("##",0,0);
        filesize = QString(array).section("##",1,1).toLongLong();
        recvsize = 0;

        //创建文件设置为只写
        file.setFileName(fileSavePath+filename);
        emit message("文件保存地址 "+fileSavePath+filename);
        bool ok = file.open(QIODevice::WriteOnly);
        if(ok == false)
        {
            file.close();
        }else
        {
            emit getFilepath(fileSavePath+filename);
        }
    }
    else//发送过来的是文件内容
    {
        qint64 len = file.write(array);
        recvsize += len;
        emit message("已接受 "+QString::number(recvsize));
        if(recvsize == filesize)
        {
            so->write("RecvDone");
            so->disconnectFromHost();
            so->waitForDisconnected();
            so->close();
            file.close();
            ss->close();
        }
    }
}

Client2::Client2(QObject *parent)
    : QObject{parent}
{
    co = new QTcpSocket;
    sotimer = new QTimer;
    connect(sotimer,&QTimer::timeout,this,[=](){
        sotimer->stop();
        sendData();
    });
}

void Client2::setPort(int port)
{
    this->port = port;
}

void Client2::connectToServer(QString IP)
{
    co->connectToHost(QHostAddress(IP),port);
    if(co->waitForConnected())
    {
        emit message("连接到了服务端");
        on_Connect();
    }
    connect(co,&QTcpSocket::readyRead,this,[=](){
        QByteArray array = co->readAll();
        if(array == "RecvDone")
        {
            file.close();
            co->disconnectFromHost();
            co->close();
            emit disconnects();
        }
    });
}

void Client2::setFilePath(QString filePath)
{
    this->filePath = filePath;
}

void Client2::sendData()
{
    qint64 len = 0;
    do{
        char buf[4*1024]{0};
        //读文件内容
        len = file.read(buf,sizeof(buf));//读了多少文件内容
        //读多少发多少
        len = co->write(buf,len);//返回已经发送多少

        sendsize += len;//已经发送多少内容
        emit message("已经发送 "+ QString::number(sendsize));
    }while(len > 0);
}

void Client2::on_Connect()
{
    QFileInfo fileinfo(filePath);
    filename = fileinfo.fileName();
    emit message("文件名 "+filename);
    filesize = fileinfo.size();
    emit message("文件大小 "+QString::number(filesize));
    sendsize = 0;
    file.setFileName(filePath);
    bool ok = file.open(QIODevice::ReadOnly);
    if(ok ==false)
    {
        emit message("文件打开失败");
        file.close();
    }
    //先发送文件信息
    QString str = QString("%1##%2").arg(filename).arg(filesize);//可在客户端解包
    QByteArray data = str.toUtf8();
    char* dat = data.data();
    emit message("头信息 " + data);
    qint64 len = co->write(dat);
    if(len > 0)//发送文件信息成功
    {
        //        发送真正的文件
        //        距离太近会出现粘包问题
        //        用定时器解决粘包问题
        sotimer->start(20);
    }
    else
    {
        file.close();
        co->disconnectFromHost();
        co->close();
    }
}
