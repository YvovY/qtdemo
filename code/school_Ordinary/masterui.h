#ifndef MASTERUI_H
#define MASTERUI_H

#include <QMouseEvent>
#include <QSqlRecord>
#include <QSqlTableModel>
#include <QStandardItemModel>
#include <QWidget>
#include"book_shelf.h"
#include"AppSocket.h"
#include"UserPie.h"
namespace Ui {
class MasterUI;
}

class MasterUI : public QWidget
{
    Q_OBJECT
private:
    UserPie* upie;
    bool suggetflag = true;
    bool userinfoflag = true;
    AppSocket* appS;
    QString tableViewBooksindex = 0;
    QString tableViewSearchInindex = 0;
    QString tableViewBookshelfindex = 0;
    int x = 0;
    int y = 0;
    int gx = 0;
    int gy = 0;
    int cx = 0;
    int cy = 0;
    int sx = 0;
    int sy = 0;
    int i = 0;
    int gi = 0;
    int ci = 0;
    int si = 0;
    QStandardItemModel* tableviewModel;
    QStandardItemModel* tableviewModelserch;
    QStandardItemModel* tableviewModelBookCollect;
    QStandardItemModel* tableviewModelSuggest;
    Book_Shelf* bookshelf;
    QSqlRecord dbrecored;
    QSqlDatabase bookdb;
    QSqlTableModel* dbmodel;
    Ui::MasterUI *ui;
    typedef struct{
        QString bookpicture;
        QString bookname;
        QString author;
        QString isbncode;
        QString publisher;
        QString status;
    }bookinfo;
    typedef struct{
        QString bookpicture;
        QString bookname;
        QString author;
        QString isbncode;
        QString publisher;
        QString status;
        bool show;
    }collectbook;
    QVector<bookinfo> Binfo;
    QVector<bookinfo> Binfoserch;
    QVector<bookinfo> Binfosuggest;
    QVector<collectbook> bookcollect;
public:
    explicit MasterUI(QWidget *parent = nullptr);
    ~MasterUI();
    void setUnmPic();
private:
    void showBookDataBase();//书本页面
    void showbookcollect();//书架页面
    void updateshowbookcollect();//书架页面
    void initOtherUI(); //初始化其他窗口构造函数
    void initFunc();//调用其他函数
    void initConnection();//链接曹与信号
    void avatars();//头像
    void names();//用户名
signals:
    void drawuserpie();
    void tableViewBooksClicked(QString index);
    void tableViewSearchInClicked(QString message);
    void tableViewBookshelfclicked(QString message);
    void LoadShow();
private slots:
    void closeEvent(QCloseEvent*);

    void on_Bookshelf_clicked();//书架按钮

    void on_BookMarket_clicked();//书城按钮

    void on_shujibaian_clicked();//书籍百谈按钮

    void on_Me_clicked();//我按钮
    void showuserinfo();//显示用户基本信息
    void showuserProfile();//显示用户画像

    void on_quitSignIn_clicked();//退出登录按钮

    void on_btGoBack_clicked();//返回按钮
    void on_tabWidget_BookMarket_tabBarClicked(int index);//书城标签点击
    void showbooksSuggest();//推荐
    void on_tableView_books_clicked(const QModelIndex &index);//某个书本点击
    void booksCollection(int index);//书本收藏
    void booksCollectionserch(int index);//搜索书本收藏
    void booksdetails();//书本详情页面
    void booksdetailserch();//搜索书本详情页面
    void on_btSearchIn_clicked();//搜索按钮
    void on_tableView_SearchIn_clicked(const QModelIndex &index);//搜索页面
    void on_tableView_Bookshelf_clicked(const QModelIndex &index);//收藏页面
    void booksBorrow(const QModelIndex &index, int sums);//归还书本
    void reloaddatabookshelf();//重新导入书架页面书本信息
};

#endif // MASTERUI_H
