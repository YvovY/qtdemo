#include "load.h"
#include "qjsondocument.h"
#include "ui_load.h"
#include"config.h"
#include <QDir>
#include <QIcon>
#include <QJsonObject>
#include <QMessageBox>
#include <QPainter>
#include <QPicture>
#include <QPixmap>
#include <QPixmap>
#include <QRect>
//#include <QSignalSpy>
#include <QSqlError>
#include <QSqlQuery>
#include <QThread>
load::load(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::load)
{
    timer = new QTimer;
    ui->setupUi(this);
    ui->AccountNumber->lineEdit()->setPlaceholderText("账号[手机号]");
    ui->btSignIn->setEnabled(false);
    ui->btSignIn->setText("正在链接。。。");
    initOtherUI();
    initConnection();
    initFunc();
    appS->on_ConnectToServer();
    connect(timer, &QTimer::timeout,this,[=](){
        timer->stop();
        ui->btSignIn->setEnabled(true);
        ui->btSignIn->setText("登录");
        masterUI->setVisible(true);
        masterUI->setUnmPic();
        this->hide();
        masterUI->show();
    });
}

load::~load()
{
    delete ui;
}

void load::initOtherUI()
{
    signup = new SignUp;
    masterUI = new MasterUI;
    forgot = new ForgotPassword(this);
    appS = &AppSocket::GetInitAppSocket();
}

void load::initFunc()
{
    wallPapers();
    logos();
    avatars();
}

void load::initConnection()
{
    connect(signup,SIGNAL(LoadShow()),this,SLOT(on_LoadShow()));
    connect(masterUI,SIGNAL(LoadShow()),this,SLOT(on_LoadShow()));
    connect(appS,&AppSocket::successCon,this,&load::on_successCon);
}

void load::wallPapers()
{
    ui->WallPaper->setText("");
    int w = ui->WallPaper->width();
    int h = ui->WallPaper->height();
    QPixmap pix;
    pix.load(wallPapers_picture_address);
    ui->WallPaper->setPixmap(pix.scaled(w,h));
}

void load::logos()
{
    ui->logo->setText("");
    int w = ui->logo->width();
    int h = ui->logo->height();
    QPixmap pix;
    pix.load(logo_picture_address);
    ui->logo->setPixmap(pix.scaled(w,h));
}

void load::avatars()
{
    ui->loadAvatar->setText("");
    int w = ui->loadAvatar->width();
    int h = ui->loadAvatar->height();
    QPixmap pix;
    pix.load(Avatar_picture_address);
    ui->loadAvatar->setPixmap(pix.scaled(w,h));
}

void load::recvDBfile()
{
    QJsonObject ob;
    ob["class"] = "load";
    ob["command"] = "getdbfile";
    Server2* recv = new Server2;
    recv->setFileSaveAddr(databaseUrl+"/");
    int port = recv->getPort();
    ob["port"] = port;
    QJsonDocument doc;
    doc.setObject(ob);
    QByteArray data = doc.toJson();
    appS->SocketWrited(data);
    QEventLoop* el = new QEventLoop();
    connect(recv, &Server2::disconnects, el, &QEventLoop::quit);
    el->exec();

}

void load::recvBookPicture()
{
    qDebug()<<"发送端口信息";
    QString savepath = QDir::currentPath()+ "/";
    QString outputpath = bookpictureUrl+"/";
    QJsonObject ob;
    ob["class"] = "load";
    ob["command"] = "getbookpicture";
    Server2* recv = new Server2;
    QString filesavepath = NULL;
    connect(recv,&Server2::getFilepath,this,[&](QString file){
        filesavepath = file;
    });
    recv->setFileSaveAddr(savepath);
    int port = recv->getPort();
    ob["port"] = port;
    QJsonDocument doc;
    doc.setObject(ob);
    QByteArray data = doc.toJson();
    appS->SocketWrited(data);
    QEventLoop* el = new QEventLoop();
    qDebug()<<"等待数据接受完毕";
    connect(recv, &Server2::disconnects, el, &QEventLoop::quit);
    el->exec();
    //解压缩文件
    qDebug()<<"解压缩";
    filezip* fzip = new filezip;
    fzip->unzip(outputpath,filesavepath);
    qDebug()<<"删除压缩文件";
    QFile::remove(filesavepath);
}

void load::on_successCon()
{
    ui->btSignIn->setEnabled(true);
    ui->btSignIn->setText("登录");
}

void load::on_LoadShow()
{
    ui->btSignIn->setText("登录");
    ui->btSignIn->setEnabled(true);
    this->show();
}


void load::on_btSignUp_clicked()
{
    signup->setVisible(true);
    this->hide();
    signup->show();
}


void load::on_btSignIn_clicked()
{
    //检查账号 和 密码 是否写入
    if(ui->AccountNumber->currentText() == NULL || ui->Password->text() == NULL)
    {
        QMessageBox::information(this,"提示","请填入账号/密码。");
        return;
    }
    ui->btSignIn->setText("正在登录。。。");
    ui->btSignIn->setEnabled(false);
    //检查数据库文件是否存在
    QDir dir;
    dir.cd(databaseUrl);
    if(dir.exists("DataBase.db3") == true)
    {
        QFile::remove(databaseUrl+"/DataBase.db3");
    }
    qDebug()<<"开始接受数据库文件";
    recvDBfile();
    qDebug()<<"数据库文件接受完毕";
    qDebug()<<"书本图像接受开始";
    recvBookPicture();
    qDebug()<<"书本图像接受完毕";
    //从数据库中查找用户是否存在，
    if(QSqlDatabase::contains("qt_sql_default_connection")) {
        bookdb = QSqlDatabase::database("qt_sql_default_connection");
    }
    else{
      bookdb = QSqlDatabase::addDatabase("QSQLITE");
    }
    QString Database = databaseUrl+"/DataBase.db3";
    bookdb.setDatabaseName(Database);
    bookdb.open();
    QSqlQuery query(bookdb);//数据库事件
    QString phonenumber = ui->AccountNumber->currentText();
    QString passwd = ui->Password->text();
    query.prepare("SELECT * FROM main.userdata WHERE phoneNumber_acountNumber = :acountNumber");
    query.bindValue(":acountNumber",phonenumber.toUtf8());
    bool ok = query.exec();
    //存在获取用户名
    QString dbaccount;
    QString dbusername;
    QString dbpasswd;
    QString dbmiwen;
    QString dbavpicture;
    if(ok)
    {
        qDebug()<<"数据库查询成功";
        while (query.next()) {
            dbaccount = query.value(0).toString();
            qDebug()<< "dbaccount" << query.value(0);
            dbusername = query.value(1).toString();
            qDebug()<< "dbusername" << query.value(1);
            dbpasswd = query.value(2).toString();
            qDebug()<< "dbpasswd" << query.value(2);
            dbmiwen = query.value(3).toString();
            qDebug()<< "dbmiwen" << query.value(3);
            dbavpicture = query.value(4).toString();
            qDebug()<< "dbavpicture" << query.value(4);
        }
        //检查是否有值
        if(dbaccount.isEmpty())
        {
            QMessageBox::information(this,"提示","此账号不存在");
            return;
        }
        setUsername(dbusername);
        setUseraccount(ui->AccountNumber->currentText());
        //检查密码是否匹配
        if(passwd != dbpasswd)
        {
            QMessageBox::information(this,"提示","密码错误");
            return;
        }
        //检查用户头像是否存在不存在从服务端获取头像图片存入相应位置
        QString fileDir = avtarpictureUrl+"/"+dbavpicture;
        QDir dir(fileDir);
        if(dir.exists())
        {
            setUserpicture(fileDir);
            //头像显示在主页面等 1秒进入主页面
            QPixmap icon(fileDir);
            ui->loadAvatar->setText("");
            ui->loadAvatar->setPixmap(QPixmap(""));
            ui->loadAvatar->setPixmap(icon.scaled(QSize(ui->loadAvatar->width()-10,ui->loadAvatar->height()-10)));
            ui->userName->setText(dbusername);
            timer->start(2000);
        }
        else
        {
            //向服务器发送用户头像不存在消息
            QJsonObject ob;
            ob["class"] = "load";
            ob["command"] = "getuserpicture";
            ob["userpicture"] = dbavpicture;
            Server2* recv = new Server2;
            recv->setFileSaveAddr(avtarpictureUrl+"/");
            int port = recv->getPort();
            ob["port"] = port;
            QJsonDocument doc;
            doc.setObject(ob);
            QByteArray data = doc.toJson();
            appS->SocketWrited(data);
            QEventLoop* el = new QEventLoop();
            connect(recv, &Server2::disconnects, el, &QEventLoop::quit);
            el->exec();
            //头像显示在主页面等 1秒进入主页面
            QPixmap icon(fileDir);
            setUserpicture(fileDir);
            ui->loadAvatar->setText("");
            ui->loadAvatar->setPixmap(QPixmap(""));
            ui->loadAvatar->setPixmap(icon.scaled(QSize(ui->loadAvatar->width()-10,ui->loadAvatar->height()-10)));
            ui->userName->setText(dbusername);
            timer->start(2000);
        }
    }
    else
    {
        qDebug()<<"数据库查询失败 "<<query.lastError().text();
    }
}


void load::on_ForgotPassword_clicked()
{
    forgot->setVisible(true);
    forgot->show();
    forgot->exec();
}
