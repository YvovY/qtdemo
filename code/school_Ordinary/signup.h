#ifndef SIGNUP_H
#define SIGNUP_H

#include <QWidget>
#include"AppSocket.h"
namespace Ui {
class SignUp;
}

class SignUp : public QWidget
{
    Q_OBJECT
private:
    QString cpfile;
    AppSocket* appS = &AppSocket::GetInitAppSocket();
    Ui::SignUp *ui;
    QString avpicture = NULL;
    QString username = NULL;
    QString passwd = NULL;
    QString phonenum = NULL;
    QString miwen = NULL;
public:
    explicit SignUp(QWidget *parent = nullptr);
    ~SignUp();
protected:
    void closeEvent(QCloseEvent*);
private:
    void logoPicture();
    bool IsValidPhoneNumber(const QString & phoneNum);
    int selectNum(QString& classVal);
    void sendavtarPicture(QJsonObject obj);
    void existUser();
signals:
    void LoadShow();
    void NewTask(const QByteArray TaskData);
private slots:
    void on_SignUpResult(QByteArray data);
    void on_AvtarPicture1_clicked();
    void on_UserName_textChanged(const QString &arg1);
    void on_Passwd_textChanged(const QString &arg1);
    void on_PhoneNumber_textChanged(const QString &arg1);
    void on_Ciphertext_textChanged(const QString &arg1);
    void on_SignUps_clicked();
};

#endif // SIGNUP_H
