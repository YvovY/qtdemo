#ifndef CONFIG_H
#define CONFIG_H
#include<QString>


extern QString wallPapers_picture_address;//图片墙图片地址
extern QString logo_picture_address;//logo图片地址
extern QString Avatar_picture_address;//头像图片地址
extern QString Username;//用户名
extern QString Useraccount;//用户账号
extern QString applicationUrl;//程序文件目录
extern QString appDataUrl;//程序数据目录
extern QString configDataUrl;//配置文件目录
extern QString databaseUrl;//数据库文件目录
extern QString pictureUrl;//图片文件目录
extern QString avtarpictureUrl;//头像图片文件目录
extern QString bookpictureUrl;//书本图片文件目录
extern QString userDataUrl;//用户数据文件目录
extern QString TempUrl;//Temp文件目录

extern QString IP;
#define APPport 9999//程序链接端口

void setUserpicture(QString Upicture);
void setUsername(QString Uname);
void setUseraccount(QString Uaccount);
#endif // CONFIG_H
