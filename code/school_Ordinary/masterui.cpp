#include "masterui.h"
#include "ui_masterui.h"
#include"config.h"
#include <QPixmap>
#include<QDebug>
#include <QSqlQuery>
#include<QSqlError>
#include <QMouseEvent>
#include <QEvent>
#include <qcoreevent.h>
#include <QMenu>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonDocument>
#include <QSet>
#include<math.h>
#include <QVector>
MasterUI::MasterUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MasterUI)
{
    ui->setupUi(this);
    appS = &AppSocket::GetInitAppSocket();
    ui->shujibaian->setVisible(false);
    ui->btGoBack->setVisible(false);
    tableviewModel = new QStandardItemModel;
    tableviewModelserch = new QStandardItemModel;
    tableviewModelBookCollect = new QStandardItemModel;
    tableviewModelSuggest = new QStandardItemModel;
    ui->tableView_books->setModel(tableviewModel);
    ui->tableView_SearchIn->setModel(tableviewModelserch);
    ui->tableView_Bookshelf->setModel(tableviewModelBookCollect);
    ui->tableView_Suggested->setModel(tableviewModelSuggest);
    connect(this,&MasterUI::tableViewBooksClicked,this,[=](QString text){
        this->tableViewBooksindex = text;
        qDebug()<<tableViewBooksindex;
    });
    connect(this,&MasterUI::tableViewSearchInClicked,this,[=](QString text){
        this->tableViewSearchInindex = text;
        qDebug()<<tableViewSearchInindex;
    });
    connect(this,&MasterUI::tableViewBookshelfclicked,this,[=](QString text){
        this->tableViewBookshelfindex = text;
        qDebug()<<tableViewBookshelfindex;
    });

    upie = new UserPie;
    connect(this,&MasterUI::drawuserpie,upie,&UserPie::drawuserpie);

    initOtherUI();
    initFunc();
    initConnection();
}

MasterUI::~MasterUI()
{
    delete ui;
}

void MasterUI::setUnmPic()
{
    avatars();
    names();
}

void MasterUI::showBookDataBase()
{
    if(Binfo.isEmpty())
    {
        if(QSqlDatabase::contains("qt_sql_default_connection")) {
            bookdb = QSqlDatabase::database("qt_sql_default_connection");
        }
        else{
            bookdb = QSqlDatabase::addDatabase("QSQLITE");
        }
        bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
        if(bookdb.open() == false)
        {
            qDebug()<<"无法打开数据库";
            return;
        }
        QSqlQuery query(bookdb);
        if(query.exec("SELECT * FROM main.xuptlibrary") == false)
        {
            qDebug()<<"query错误 "<<query.lastError().text();
            return;
        }
        while (query.next()) {
            bookinfo b;
            b.bookname = "书名:"+query.value(0).toString();
            b.isbncode = "ISBN码:"+query.value(1).toString();
            b.bookpicture = bookpictureUrl+"/"+query.value(2).toString();
            b.author = "作者:"+query.value(3).toString();
            b.publisher = "出版社:"+query.value(4).toString();
            b.status = "预览";
            Binfo.push_back(b);
            if(i!=0 && i%3==0)
            {
                x++;
                y = 0;
            }
            Book_Shelf* bs = new Book_Shelf(ui->tableView_books);
            QStandardItem* item = new QStandardItem();
            tableviewModel->setItem(x, y, item);
            ui->tableView_books->setIndexWidget(tableviewModel->index(x,y), bs->getbookshelf(Binfo[i].bookpicture,Binfo[i].bookname,Binfo[i].author,Binfo[i].isbncode,Binfo[i].publisher,Binfo[i].status));
            y++;
            i++;
        }
    }
}

void MasterUI::showbookcollect()
{
    if(bookcollect.isEmpty())
    {
        //    获取用户收藏的书本名称
        QStringList booknamelist;
        if(QSqlDatabase::contains("qt_sql_default_connection")) {
            bookdb = QSqlDatabase::database("qt_sql_default_connection");
        }
        else{
            bookdb = QSqlDatabase::addDatabase("QSQLITE");
        }
        bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
        if(bookdb.open() == false)
        {
            qDebug()<<"无法打开数据库";
            return;
        }
        QSqlQuery query(bookdb);
        query.prepare("SELECT BookName FROM main.booktableCollection WHERE UserName = :UserName");
        query.bindValue(":UserName",Username.toUtf8());
        if(query.exec() == false)
        {
            qDebug()<<"query错误 "<<query.lastError().text();
            return;
        }
        while (query.next()) {
            booknamelist << query.value(0).toString();
        }
        qDebug()<<"booknamelist:";
        qDebug()<<booknamelist;
        //    从书本vector获取书本信息存入收藏vector里
        for(int i = 0; i < booknamelist.size(); i++)
        {
            for(int j = 0; j < Binfo.size(); j++)
            {
                QString booklist = booknamelist[i];
                QString bookname = Binfo[j].bookname;
                bookname = bookname.section(":",1,1);
                qDebug()<<"booklist:"<<booklist;
                qDebug()<<"bookname:"<<bookname;
                if(booklist == bookname)
                {
                    qDebug()<<booklist<<" = "<<bookname;
                    collectbook col;
                    col.bookname = Binfo[i].bookname;
                    col.author = Binfo[i].author;
                    col.bookpicture = Binfo[i].bookpicture;
                    col.isbncode = Binfo[i].isbncode;
                    col.publisher = Binfo[i].publisher;
                    col.status = "借阅\n时间:\n30天";
                    col.show = false;
                    bookcollect.push_back(col);
                }
            }
        }
        //    展示在书架页面
        for(int i = 0; i < bookcollect.size(); i++)
        {
            if(bookcollect[i].show == false)
            {
                if(ci!=0 && ci%3==0)
                {
                    cx++;
                    cy = 0;
                }
                Book_Shelf* bs = new Book_Shelf(ui->tableView_Bookshelf);
                QStandardItem* item = new QStandardItem();
                tableviewModelBookCollect->setItem(cx, cy, item);
                ui->tableView_Bookshelf->setIndexWidget(tableviewModelBookCollect->index(cx,cy), bs->getbookshelf(bookcollect[i].bookpicture,bookcollect[i].bookname,bookcollect[i].author,bookcollect[i].isbncode,bookcollect[i].publisher,bookcollect[i].status));
                cy++;
                ci++;
            }
        }
    }
    qDebug()<<"bookcollect size:"<<bookcollect.size();
}

void MasterUI::updateshowbookcollect()
{
    cx = 0;
    cy = 0;
    ci = 0;
    //    展示在书架页面
    for(int i = 0; i < bookcollect.size(); i++)
    {
        if(bookcollect[i].show == false)
        {
            if(ci!=0 && ci%3==0)
            {
                cx++;
                cy = 0;
            }
            Book_Shelf* bs = new Book_Shelf(ui->tableView_Bookshelf);
            QStandardItem* item = new QStandardItem();
            tableviewModelBookCollect->setItem(cx, cy, item);
            ui->tableView_Bookshelf->setIndexWidget(tableviewModelBookCollect->index(cx,cy), bs->getbookshelf(bookcollect[i].bookpicture,bookcollect[i].bookname,bookcollect[i].author,bookcollect[i].isbncode,bookcollect[i].publisher,bookcollect[i].status));
            cy++;
            ci++;
        }
    }
    qDebug()<<"bookcollect size:"<<bookcollect.size();
}
void MasterUI::initOtherUI()
{
}

void MasterUI::initFunc()
{
    avatars();
    names();
    ui->stackedWidget_show->setCurrentIndex(1);
    ui->tabWidget_BookMarket->setCurrentIndex(1);
    showBookDataBase();
}

void MasterUI::initConnection()
{
}

void MasterUI::avatars()
{
    ui->AvtarPicture->setText("");
    int w = ui->AvtarPicture->width();
    int h = ui->AvtarPicture->height();
    QPixmap pix;
    pix.load(Avatar_picture_address);
    qDebug()<<"用户头像 "<<Avatar_picture_address;
    ui->AvtarPicture->setPixmap(pix.scaled(w,h));
}

void MasterUI::names()
{
    qDebug()<<"用户名 "<<Username;
    ui->userName->setText(Username);
}

void MasterUI::on_Bookshelf_clicked()
{
    ui->stackedWidget_show->setCurrentIndex(0);
    showbookcollect();
}


void MasterUI::on_BookMarket_clicked()
{
    ui->stackedWidget_show->setCurrentIndex(1);
    ui->tabWidget_BookMarket->setCurrentIndex(1);
    showBookDataBase();
}


void MasterUI::on_shujibaian_clicked()
{
    ui->stackedWidget_show->setCurrentIndex(2);
}


void MasterUI::on_Me_clicked()
{
    emit drawuserpie();
    ui->stackedWidget_show->setCurrentIndex(3);
    showuserinfo();
    showuserProfile();
}

void MasterUI::showuserinfo()
{
    if(userinfoflag)
    {
        QStringList booknamelist;
        if(QSqlDatabase::contains("qt_sql_default_connection")) {
            bookdb = QSqlDatabase::database("qt_sql_default_connection");
        }
        else{
            bookdb = QSqlDatabase::addDatabase("QSQLITE");
        }
        bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
        if(bookdb.open() == false)
        {
            qDebug()<<"无法打开数据库";
            return;
        }
        QSqlQuery query(bookdb);
        query.prepare("SELECT * FROM main.userdata WHERE userName = :userName");
        query.bindValue(":userName",Username.toUtf8());
        if(query.exec() == false)
        {
            qDebug()<<"query错误 "<<query.lastError().text();
            return;
        }
        while (query.next()) {
            ui->label_useraccountnumber->setText("账号："+query.value(0).toString());
            ui->label_username->setText("用户名："+query.value(1).toString());
            ui->label_userpasswd->setText("密码："+query.value(2).toString());
            ui->label_usermiwen->setText("密文："+query.value(3).toString());
            int w = ui->label_avpictures->width();
            int h = ui->label_avpictures->height();
            QPixmap pix;
            pix.load(Avatar_picture_address);
            ui->label_avpictures->setPixmap(pix.scaled(w,h));
        }
        userinfoflag = false;
    }
}

void MasterUI::showuserProfile()
{
    //画饼图
    //读取用户记录数据文件分析数据
    //    求出总和分数
    //    把每个分数/分数总和
    //    按照结果画饼图
}

void MasterUI::on_btGoBack_clicked()
{
    ui->stackedWidget_show->setCurrentIndex(1);
    ui->btGoBack->setVisible(false);
    ui->BookMarket->setVisible(true);
    ui->Bookshelf->setVisible(true);
    ui->Me->setVisible(true);
    ui->INbookName->clear();
    ui->INISBNcode->clear();
    ui->bookPicture->setPixmap(QPixmap(""));
    ui->INauthor->clear();
    ui->INpublisher->clear();
    ui->INfloor->clear();
    ui->INbookRoom->clear();
    ui->INbookrack->clear();
    ui->INrows->clear();
    ui->BookDetails->clear();
}

void MasterUI::closeEvent(QCloseEvent*){}

void MasterUI::on_quitSignIn_clicked()
{
    emit LoadShow();
    this->setVisible(false);
    this->hide();
}


void MasterUI::on_tabWidget_BookMarket_tabBarClicked(int index)
{
    switch (index) {
    case 0:
        showbooksSuggest();
        break;
    case 1:
        showBookDataBase();
        break;
    case 2:
        break;
    case 3:
        break;
    }
}

void MasterUI::showbooksSuggest()
{

    if(suggetflag){
        tableviewModelSuggest->clear();
        if(!Binfosuggest.isEmpty())
        {
            QVector<bookinfo>().swap(Binfosuggest);
        }
        //    获取书本信息进行计算保存到本地文件
        //获取用户名单
        qDebug()<<"开始计算！！！！";
        QSet<QString> userlist;
        if(QSqlDatabase::contains("qt_sql_default_connection")) {
            bookdb = QSqlDatabase::database("qt_sql_default_connection");
        }
        else{
            bookdb = QSqlDatabase::addDatabase("QSQLITE");
        }
        bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
        if(bookdb.open() == false)
        {
            qDebug()<<"无法打开数据库";
            return;
        }
        QSqlQuery query(bookdb);
        if(query.exec("SELECT UserName FROM main.booktableCollection"))
        {
            while (query.next()) {
                userlist.insert(query.value(0).toString());
            }
        }
        else
        {
            qDebug()<<"获取用户名单失败："<<query.lastError().text();
            return;
        }
        //生成 用户-物品的对应表
        QMap<QString,QSet<QString>> user_to_item;
        for(auto i = userlist.begin(); i != userlist.end(); i++)
        {
            QSet<QString> booklist;
            //        按用户名获取书本集合
            QString nameuser = *i;
            qDebug()<<nameuser;
            query.prepare("SELECT BookName FROM main.booktableCollection WHERE UserName = :UserName");
            query.bindValue(":UserName",nameuser.toUtf8());
            if(query.exec())
            {
                while(query.next())
                {
                    QString namebook = query.value(0).toString();
                    qDebug()<<namebook;
                    booklist.insert(namebook);
                }
            }
            else
            {
                qDebug()<<"获取书本名单失败："<<query.lastError().text();
                return;
            }
            user_to_item[nameuser] = booklist;
        }
        if(user_to_item.isEmpty())
        {
            qDebug()<<"用户-物品表为空";
            return;
        }
        //用户-物品 转换 物品-用户 倒排表
        QMap<QString,QSet<QString>> item_to_user;
        QMap<QString,QSet<QString>>::Iterator it_itu = user_to_item.begin();
        while(it_itu != user_to_item.end())
        {
            QSet<QString>& item_set = it_itu.value();
            for(auto i = item_set.begin(); i != item_set.end(); i++)
            {
                item_to_user[*i].insert(it_itu.key());
            }
            it_itu++;
        }
        if(item_to_user.isEmpty())
        {
            qDebug()<<"物品-用户表为空";
            return;
        }
        //建立稀疏矩阵
        QMap<QString,QMap<QString,double>> CoRated_table;
        for(auto i = user_to_item.begin(); i != user_to_item.end(); i++)
        {
            QSet<QString>& user_set = i.value();
            for(auto set_begin1 = user_set.begin(); set_begin1 != user_set.end(); set_begin1++)
            {
                QString user1 = *set_begin1;
                for(auto set_begin2 = user_set.begin(); set_begin2 != user_set.end(); set_begin2++)
                {
                    QString user2 = *set_begin2;
                    if(user1 == user2)
                    {
                        CoRated_table[user1][user2] = 0;
                        continue;
                    }
                    bool init = false;
                    QMap<QString,QMap<QString,double>>::Iterator aim1 = CoRated_table.find(user1);
                    if(aim1 != CoRated_table.end())
                    {
                        QMap<QString,double>& temp = aim1.value();
                        QMap<QString,double>::Iterator aim2 = temp.find(user2);
                        if(aim2 != temp.end())
                        {
                            init = true;
                        }
                    }
                    if(!init)
                    {
                        CoRated_table[user1][user2] = 1;
                    }
                    else
                    {
                        CoRated_table[user1][user2]++;
                    }
                }
            }
        }
        if(CoRated_table.isEmpty())
        {
            qDebug()<<"稀疏矩阵为空";
            return;
        }
        //计算物品之间的相似度
        QMap<QString,QMap<QString,double>> similarity;
        //    CoRated_table,item_to_user, similarity
        for(auto iter_corated1 = CoRated_table.begin(); iter_corated1 != CoRated_table.end(); iter_corated1++)
        {
            const QString& user1 = iter_corated1.key();
            const QMap<QString,double>& table = iter_corated1.value();
            int size1 = item_to_user[user1].size();
            for(auto iter_corated2 = table.begin(); iter_corated2 != table.end(); iter_corated2++)
            {
                const QString& user2 = iter_corated2.key();
                const double score = iter_corated2.value();
                int size2 = item_to_user[user2].size();
                if(user1 == user2)
                {
                    similarity[user1][user2] = 0;
                }
                else
                {
                    similarity[user1][user2] = (double)score / sqrt(size1 * size2);
                }
            }
        }
        if(similarity.isEmpty())
        {
            qDebug()<<"相似度结果矩阵为空";
            return;
        }
        //输出物品之间的相似度
        QVector<QVector<QPair<QString, double>>> result_vec;
        for(auto it = similarity.begin(); it != similarity.end(); it++)
        {
            QVector<QPair<QString, double>> vec;
            for(auto itm = it.value().begin(); itm != it.value().end(); itm++)
            {
                if(itm.value() != 0 && itm.value() != 1)
                {
                    vec.push_back(qMakePair(itm.key(),itm.value()));
                }
                result_vec.push_back(vec);
            }
        }
        if(result_vec.isEmpty())
        {
            qDebug()<<"相似度结果矩阵为空[vector]";
            return;
        }
        //给指定用户推荐物品
        QSet<QString> rank;
        QVector<QString> item;
        for(auto i = user_to_item.begin(); i != user_to_item.end(); i++)
        {
            if(i.key() == Username)//找到用户的数据
            {
                for(auto ii = i.value().begin(); ii != i.value().end(); ii++)
                {
                    item.push_back(*ii);//把用户数组的值插入临时变量item
                    //从similarity中查找ii
                    for(auto iii = similarity.begin(); iii != similarity.end(); iii++)
                    {
                        if(iii.key() == *ii)//找到ii的相似物品
                        {
                            for(auto iiii = iii.value().begin(); iiii != iii.value().end(); iiii++)
                            {
                                if(iiii.value() != 0 && iiii.value() != 1 &&iiii.value() >= 0.5)
                                {
                                    rank.insert(iiii.key());
                                }
                            }
                        }
                    }
                }
            }
        }
        QVector<QString> v;
        for(auto it = rank.begin(); it != rank.end(); it++)
        {
            v.push_back(*it);
        }
        qDebug()<<"item大小: "<<item.size()<<" v大小："<<v.size();
        for(int i = 0; i < item.size(); i++)
        {
            for(int j = 0; j < v.size(); j++)
            {
                if(item[i] == v[j])
                {
                    qDebug()<<item[i]<<" = "<<v[j];
                    v.remove(j,1);
                    break;
                }
            }
        }
        qDebug()<<"推荐：";
        for(int i = 0; i < v.size(); i++)
        {
            qDebug()<<v[i]<<" ";
        }
        //推荐计算完毕
        qDebug()<<"计算完毕！！！！";
        qDebug()<<"展示在ui界面上";
        //展示在ui界面上
        for(int it1 = 0; it1 < v.size(); it1++)
        {
            for(int it2 = 0; it2 < Binfo.size(); it2++)
            {
                QString it1name = v[it1];
                QString it2name = Binfo[it2].bookname;
                it2name = it2name.remove(0,3);
                qDebug()<<it1name<<"&&"<<it2name;
                if(it1name == it2name)
                {
                    qDebug()<<it1name<<" == "<<it2name;
                    bookinfo b;
                    b.bookname =Binfo[it2].bookname;
                    b.author =Binfo[it2].author;
                    b.bookpicture =Binfo[it2].bookpicture;
                    b.isbncode =Binfo[it2].isbncode;
                    b.publisher =Binfo[it2].publisher;
                    b.status = "推荐";
                    Binfosuggest.push_back(b);
                }
            }
        }
        gx = 0;
        gy = 0;
        gi = 0;
        //    展示在书架页面
        for(int i = 0; i < Binfosuggest.size(); i++)
        {
            if(gi!=0 && gi%3==0)
            {
                gx++;
                gy = 0;
            }
            Book_Shelf* bs = new Book_Shelf(ui->tableView_Suggested);
            QStandardItem* item = new QStandardItem();
            tableviewModelSuggest->setItem(gx, gy, item);
            ui->tableView_Suggested->setIndexWidget(tableviewModelSuggest->index(gx,gy), bs->getbookshelf(Binfosuggest[i].bookpicture,Binfosuggest[i].bookname,Binfosuggest[i].author,Binfosuggest[i].isbncode,Binfosuggest[i].publisher,Binfosuggest[i].status));
            gy++;
            gi++;
        }
        qDebug()<<"展示完毕";
    }
    suggetflag = false;
    qDebug()<<"Binfosuggest size: "<<Binfosuggest.size();
}


void MasterUI::on_tableView_books_clicked(const QModelIndex &index)
{
    int x,y;
    x = index.row()+1;
    y = index.column()+1;
    int sumss;
    if(x > 1)
    {
        if(y <3)
        {
            sumss = (x-1)*3 +y;
        }
        else
        {
            sumss  = x*y;
        }
    }
    else
    {
        sumss = y;
    }
    QString text = "tableView_books##"+QString::number(sumss);
    emit tableViewBooksClicked(text);
    QMenu* menu = new QMenu(this);
    QAction* actions = new QAction("借阅");
    QAction* actionx = new QAction("详情");
    connect(actions,&QAction::triggered,this,[=](){
        this->booksCollection(sumss);
    });
    connect(actionx,&QAction::triggered,this,&MasterUI::booksdetails);
    menu->addAction(actions);
    menu->addAction(actionx);
    menu->exec(QCursor::pos());

}

void MasterUI::booksCollection(int index)
{
    //获取书本信息保存到本地文件
    QString dir = TempUrl +"/"+Username+".txt";
    qDebug()<<dir;
    QFile file(dir);
    file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
    QString bookname = Binfo[index-1].bookname;
    bookname = bookname.remove(0,3);
    if(QSqlDatabase::contains("qt_sql_default_connection")) {
        bookdb = QSqlDatabase::database("qt_sql_default_connection");
    }
    else{
        bookdb = QSqlDatabase::addDatabase("QSQLITE");
    }
    bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
    if(bookdb.open() == false)
    {
        qDebug()<<"无法打开数据库";
        return;
    }
    QSqlQuery query(bookdb);
    if(query.prepare("SELECT * FROM main.xuptlibrary WHERE BookName LIKE :BookName") == false)
    {
        qDebug()<<"query prepare错误 "<<query.lastError().text();
    }
    query.bindValue(":BookName",bookname.toUtf8());
    if(query.exec() == false)
    {
        qDebug()<<"query exec错误 "<<query.lastError().text();
        return;
    }
    QString booktype;
    while (query.next()) {
        booktype = query.value(5).toString();
    }
    QString text = Username+ "#"+bookname+"#"+booktype+"#"+"0.1";
    qDebug()<<text;
    file.write(text.toUtf8()+"\n");
    //把信息写入到相关的vector里
    bool vecflag = true;
    for(auto i = bookcollect.begin(); i != bookcollect.end(); i++)
    {
        if(i->bookname == Binfo[index-1].bookname)
        {
            vecflag = false;
        }
    }
    if(vecflag)
    {
        QVector<bookinfo> temp;
        temp.push_back(Binfo[index-1]);
        collectbook col;
        col.bookname = temp[0].bookname;
        col.author = temp[0].author;
        col.bookpicture = temp[0].bookpicture;
        col.isbncode = temp[0].isbncode;
        col.publisher = temp[0].publisher;
        col.status = "借阅\n时间:\n30天";
        col.show = false;
        bookcollect.push_back(col);
        QVector<bookinfo>().swap(temp);
    }
    //把相关的数据发送给服务端
    QJsonObject obj;
    obj["class"] = "MasterUI";
    obj["command"] = "Collectionnewbook";
    obj["text"] = text;
    QJsonDocument doc;
    doc.setObject(obj);
    QByteArray data = doc.toJson();
    appS->SocketWrited(data);
    file.close();
    //    写入到本地数据库中
    query.prepare("SELECT UserName FROM main.booktableCollection WHERE BookName = :BookName");
    query.bindValue(":BookName",bookname.toUtf8());
    query.exec();
    QStringList valList;
    while (query.next()) {
        valList << query.value(0).toString();
    }
    bool flags = true;
    for(int i = 0; i < valList.size(); i++)
    {
        if(valList[i] == Username)
        {
            flags = false;
        }
    }
    if(flags)
    {
        qDebug()<<"此纪录不存在加入进去了";
        suggetflag = true;
        query.prepare("INSERT INTO main.booktableCollection (UserName,BookName) VALUES(:UserName,:BookName)");
        query.bindValue(":UserName",Username.toUtf8());
        query.bindValue(":BookName",bookname.toUtf8());
        bool ok = query.exec();
        if(!ok)
        {
            qDebug()<<"querry运行失败！["<<query.lastError().text()<<"]";
        }
    }
    else
    {
        qDebug()<<"此纪录存在不加入进去了";
    }
    updateshowbookcollect();
}

void MasterUI::booksCollectionserch(int index)
{
    //获取书本信息保存到本地文件
    QString dir = TempUrl +"/"+Username+".txt";
    qDebug()<<dir;
    QFile file(dir);
    file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
    QString bookname = Binfoserch[index-1].bookname;
    bookname = bookname.remove(0,3);
    if(QSqlDatabase::contains("qt_sql_default_connection")) {
        bookdb = QSqlDatabase::database("qt_sql_default_connection");
    }
    else{
        bookdb = QSqlDatabase::addDatabase("QSQLITE");
    }
    bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
    if(bookdb.open() == false)
    {
        qDebug()<<"无法打开数据库";
        return;
    }
    QSqlQuery query(bookdb);
    if(query.prepare("SELECT * FROM main.xuptlibrary WHERE BookName LIKE :BookName") == false)
    {
        qDebug()<<"query prepare错误 "<<query.lastError().text();
    }
    query.bindValue(":BookName",bookname.toUtf8());
    if(query.exec() == false)
    {
        qDebug()<<"query exec错误 "<<query.lastError().text();
        return;
    }
    QString booktype;
    while (query.next()) {
        booktype = query.value(5).toString();
    }
    QString text = Username+ "#"+bookname+"#"+booktype+"#"+"0.2";
    qDebug()<<text;
    file.write(text.toUtf8()+"\n");
    //把信息写入到相关的vector里
    bool vecflag = true;
    for(auto i = bookcollect.begin(); i != bookcollect.end(); i++)
    {
        if(i->bookname == Binfoserch[index-1].bookname)
        {
            vecflag = false;
        }
    }
    if(vecflag)
    {
        QVector<bookinfo> temp;
        temp.push_back(Binfoserch[index-1]);
        collectbook col;
        col.bookname = temp[0].bookname;
        col.author = temp[0].author;
        col.bookpicture = temp[0].bookpicture;
        col.isbncode = temp[0].isbncode;
        col.publisher = temp[0].publisher;
        col.status = "借阅\n时间:\n30天";
        col.show = false;
        bookcollect.push_back(col);
        QVector<bookinfo>().swap(temp);
    }
    //把相关的数据发送给服务端
    QJsonObject obj;
    obj["class"] = "MasterUI";
    obj["command"] = "Collectionnewbook";
    obj["text"] = text;
    QJsonDocument doc;
    doc.setObject(obj);
    QByteArray data = doc.toJson();
    appS->SocketWrited(data);
    file.close();
    //    写入到本地数据库中
    query.prepare("SELECT UserName FROM main.booktableCollection WHERE BookName = :BookName");
    query.bindValue(":BookName",bookname.toUtf8());
    query.exec();
    QStringList valList;
    while (query.next()) {
        valList << query.value(0).toString();
    }
    bool flags = true;
    for(int i = 0; i < valList.size(); i++)
    {
        if(valList[i] == Username)
        {
            flags = false;
        }
    }
    if(flags)
    {
        qDebug()<<"此纪录不存在加入进去了";
        suggetflag = true;
        query.prepare("INSERT INTO main.booktableCollection (UserName,BookName) VALUES(:UserName,:BookName)");
        query.bindValue(":UserName",Username.toUtf8());
        query.bindValue(":BookName",bookname.toUtf8());
        bool ok = query.exec();
        if(!ok)
        {
            qDebug()<<"querry运行失败！["<<query.lastError().text()<<"]";
        }
    }
    else
    {
        qDebug()<<"此纪录存在不加入进去了";
    }
    updateshowbookcollect();
}

void MasterUI::booksdetails()
{
    //    跳到详情页面
    if(QSqlDatabase::contains("qt_sql_default_connection")) {
        bookdb = QSqlDatabase::database("qt_sql_default_connection");
    }
    else{
        bookdb = QSqlDatabase::addDatabase("QSQLITE");
    }
    bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
    if(bookdb.open() == false)
    {
        qDebug()<<"无法打开数据库";
        return;
    }
    int index = tableViewBooksindex.section("##",1,1).toInt();
    qDebug()<<"index "<<index;
    QString bookn = Binfo[index -1].bookname;
    bookn = bookn.section(":",1,1);
    qDebug()<<bookn;
    QSqlQuery query(bookdb);
    if(query.prepare("SELECT * FROM main.xuptlibrary WHERE BookName LIKE :BookName") == false)
    {
        qDebug()<<"query prepare错误 "<<query.lastError().text();
    }
    query.bindValue(":BookName",bookn.toUtf8());
    if(query.exec() == false)
    {
        qDebug()<<"query exec错误 "<<query.lastError().text();
        return;
    }
    while (query.next()) {
        ui->INbookName->setText(query.value(0).toString());
        ui->INISBNcode->setText(query.value(1).toString());
        QPixmap pix(bookpictureUrl+"/"+query.value(2).toString());
        ui->bookPicture->setPixmap(pix.scaled(ui->bookPicture->width(),ui->bookPicture->height()));
        ui->INauthor->setText(query.value(3).toString());
        ui->INpublisher->setText(query.value(4).toString());
        ui->INfloor->setText(query.value(6).toString());
        ui->INbookRoom->setText(query.value(7).toString());
        ui->INbookrack->setText(query.value(8).toString());
        ui->INrows->setText(query.value(9).toString()+"/"+query.value(10).toString());
        ui->BookDetails->appendPlainText(query.value(11).toString());
    }
    ui->stackedWidget_show->setCurrentIndex(4);
    ui->btGoBack->setVisible(true);
    ui->BookMarket->setVisible(false);
    ui->Bookshelf->setVisible(false);
    ui->Me->setVisible(false);
}

void MasterUI::booksdetailserch()
{
    //    跳到详情页面
    if(QSqlDatabase::contains("qt_sql_default_connection")) {
        bookdb = QSqlDatabase::database("qt_sql_default_connection");
    }
    else{
        bookdb = QSqlDatabase::addDatabase("QSQLITE");
    }
    bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
    if(bookdb.open() == false)
    {
        qDebug()<<"无法打开数据库";
        return;
    }
    int index = tableViewSearchInindex.section("##",1,1).toInt();
    qDebug()<<"index "<<index;
    QString bookn = Binfoserch[index -1].bookname;
    bookn = bookn.section(":",1,1);
    qDebug()<<bookn;
    QSqlQuery query(bookdb);
    if(query.prepare("SELECT * FROM main.xuptlibrary WHERE BookName LIKE :BookName") == false)
    {
        qDebug()<<"query prepare错误 "<<query.lastError().text();
    }
    query.bindValue(":BookName",bookn.toUtf8());
    if(query.exec() == false)
    {
        qDebug()<<"query exec错误 "<<query.lastError().text();
        return;
    }
    qDebug()<<"数据展示";
    while (query.next()) {
        ui->INbookName->setText(query.value(0).toString());
        ui->INISBNcode->setText(query.value(1).toString());
        QPixmap pix(bookpictureUrl+"/"+query.value(2).toString());
        ui->bookPicture->setPixmap(pix.scaled(ui->bookPicture->width(),ui->bookPicture->height()));
        ui->INauthor->setText(query.value(3).toString());
        ui->INpublisher->setText(query.value(4).toString());
        ui->INfloor->setText(query.value(6).toString());
        ui->INbookRoom->setText(query.value(7).toString());
        ui->INbookrack->setText(query.value(8).toString());
        ui->INrows->setText(query.value(9).toString()+"/"+query.value(10).toString());
        ui->BookDetails->appendPlainText(query.value(11).toString());
    }
    ui->stackedWidget_show->setCurrentIndex(4);
    ui->btGoBack->setVisible(true);
    ui->BookMarket->setVisible(false);
    ui->Bookshelf->setVisible(false);
    ui->Me->setVisible(false);
}



void MasterUI::on_btSearchIn_clicked()
{
    if(QSqlDatabase::contains("qt_sql_default_connection")) {
        bookdb = QSqlDatabase::database("qt_sql_default_connection");
    }
    else{
        bookdb = QSqlDatabase::addDatabase("QSQLITE");
    }
    bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
    if(bookdb.open() == false)
    {
        qDebug()<<"无法打开数据库";
        return;
    }
    if(Binfoserch.isEmpty() == false)
    {
        QVector<bookinfo>().swap(Binfoserch);
    }
    tableviewModelserch->clear();
    sx = 0;
    sy = 0;
    si = 0;
    if(ui->lineEdit_SearchIn->text() == NULL)
    {
        QMessageBox::information(this,"提示","请填入输入框！");
        return;
    }
    ui->messagess_SearchIn->setText("正在搜索请稍候。。。");
    QString bookname = ui->lineEdit_SearchIn->text();
    QSqlQuery query(bookdb);
    QString code = "%"+bookname+"%";
    query.prepare("SELECT * FROM main.xuptlibrary WHERE BookName LIKE :BookName");
    query.bindValue(":BookName",code.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        ui->lineEdit_SearchIn->clear();
        ui->messagess_SearchIn->setText("搜索完毕！");
        while(query.next())
        {
            bookinfo b;
            b.bookname = "书名:"+query.value(0).toString();
            b.isbncode = "ISBN码:"+query.value(1).toString();
            b.bookpicture = bookpictureUrl+"/"+query.value(2).toString();
            b.author = "作者:"+query.value(3).toString();
            b.publisher = "出版社:"+query.value(4).toString();
            b.status = "预览";
            Binfoserch.push_back(b);
            if(si!=0 && si%3==0)
            {
                sx++;
                sy = 0;
            }
            qDebug()<<"展示搜索到的数据 "<<Binfoserch.size();
            Book_Shelf* bs = new Book_Shelf(ui->tableView_SearchIn);
            QStandardItem* item = new QStandardItem();
            tableviewModelserch->setItem(sx, sy, item);
            ui->tableView_SearchIn->setIndexWidget(tableviewModelserch->index(sx,sy), bs->getbookshelf(Binfoserch[si].bookpicture,Binfoserch[si].bookname,Binfoserch[si].author,Binfoserch[si].isbncode,Binfoserch[si].publisher,Binfoserch[si].status));
            sy++;
            si++;
        }

    }else
    {
        qDebug()<<"搜索失败！["<<query.lastError().text()<<"]";
    }
    bookdb.close();
}


void MasterUI::on_tableView_SearchIn_clicked(const QModelIndex &index)
{
    int x,y;
    x = index.row()+1;
    y = index.column()+1;
    int sumss;
    if(x > 1)
    {
        if(y <3)
        {
            sumss = (x-1)*3 +y;
        }
        else
        {
            sumss  = x*y;
        }
    }
    else
    {
        sumss = y;
    }
    QString text = "tableView_SearchIn##"+QString::number(sumss);

    emit tableViewSearchInClicked(text);
    QMenu* menu = new QMenu(this);
    QAction* actions = new QAction("借阅");
    QAction* actionx = new QAction("详情");
    connect(actions,&QAction::triggered,this,[=](){
        this->booksCollectionserch(sumss);
    });
    connect(actionx,&QAction::triggered,this,&MasterUI::booksdetailserch);
    menu->addAction(actions);
    menu->addAction(actionx);
    menu->exec(QCursor::pos());
}


void MasterUI::on_tableView_Bookshelf_clicked(const QModelIndex &index)
{
    int x,y;
    x = index.row()+1;
    y = index.column()+1;
    int sumss;
    if(x > 1)
    {
        if(y <3)
        {
            sumss = (x-1)*3 +y;
        }
        else
        {
            sumss  = x*y;
        }
    }
    else
    {
        sumss = y;
    }
    QString text = "tableView_SearchIn##"+QString::number(sumss);
    emit tableViewBookshelfclicked(text);
    QMenu* menu = new QMenu(this);
    QAction* actionj = new QAction("归还");
    connect(actionj,&QAction::triggered,this,[=](){
        this->booksBorrow(index, sumss);
    });
    menu->addAction(actionj);
    menu->exec(QCursor::pos());
}

void MasterUI::booksBorrow(const QModelIndex &index, int sums)
{
    QVector<collectbook> temp;
    temp.push_back(bookcollect[sums -1]);
    //    从数据库删除此记录
    if(QSqlDatabase::contains("qt_sql_default_connection")) {
        bookdb = QSqlDatabase::database("qt_sql_default_connection");
    }
    else{
        bookdb = QSqlDatabase::addDatabase("QSQLITE");
    }
    bookdb.setDatabaseName(databaseUrl+"/DataBase.db3");
    if(bookdb.open() == false)
    {
        qDebug()<<"无法打开数据库";
        return;
    }
    QSqlQuery query(bookdb);
    query.prepare("DELETE FROM main.booktableCollection WHERE BookName=:BookName AND UserName=:UserName");
    query.bindValue(":BookName",temp[0].bookname.toUtf8());
    query.bindValue(":UserName",Username.toUtf8());
    bool ok = query.exec();
    if(ok)
    {
        qDebug()<<"成功删除此纪录";
    }
    else
    {
        qDebug()<<"没能删除此纪录:"<<query.lastError().text();
        return;
    }
    //    从vector里删除此书
    bookcollect.removeAt(sums -1);
    //    刷新页面
    tableviewModelBookCollect->clear();
    reloaddatabookshelf();
    //把相关的数据发送给服务端
    QString text = Username+"##"+temp[0].bookname;
    QJsonObject obj;
    obj["class"] = "MasterUI";
    obj["command"] = "DeleteCollectionBook";
    obj["text"] = text;
    QJsonDocument doc;
    doc.setObject(obj);
    QByteArray data = doc.toJson();
    appS->SocketWrited(data);
}

void MasterUI::reloaddatabookshelf()
{
    cx = 0;
    cy = 0;
    ci = 0;
    //    展示在书架页面
    for(int i = 0; i < bookcollect.size(); i++)
    {
        //        if(bookcollect[i].show == false)
        {
            if(ci!=0 && ci%3==0)
            {
                cx++;
                cy = 0;
            }
            Book_Shelf* bs = new Book_Shelf(ui->tableView_Bookshelf);
            QStandardItem* item = new QStandardItem();
            tableviewModelBookCollect->setItem(cx, cy, item);
            ui->tableView_Bookshelf->setIndexWidget(tableviewModelBookCollect->index(cx,cy), bs->getbookshelf(bookcollect[i].bookpicture,bookcollect[i].bookname,bookcollect[i].author,bookcollect[i].isbncode,bookcollect[i].publisher,bookcollect[i].status));
            cy++;
            ci++;
        }
    }
    qDebug()<<"bookcollect size:"<<bookcollect.size();
}

