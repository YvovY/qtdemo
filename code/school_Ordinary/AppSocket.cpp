#include "AppSocket.h"
#include<QDebug>
#include <QHostAddress>
#include <QJsonDocument>
#include <QJsonObject>

AppSocket::~AppSocket()
{
    qDebug()<<"AppSocket析构函数";
    delete socket;
}

AppSocket &AppSocket::GetInitAppSocket()
{
    static AppSocket appSocket;
    return appSocket;
}

void AppSocket::SocketWrited(const QByteArray &data)
{
    int ok = socket->write(data);
    if(ok == -1)
    {
        qDebug()<<"数据发送失败！["<<socket->errorString()<<"]";
    }
}

AppSocket::AppSocket(QObject *parent)
    : QObject{parent}
{
    classList<<"SignUp";
    qDebug()<<"AppSocket构造函数";
    socket = new QTcpSocket;
    connect(socket,&QTcpSocket::readyRead,this,&AppSocket::on_readedMessage);
}

int AppSocket::num(QString classVal)
{
    for(int i = 0; i < classList.size(); i++)
    {
        if(classList[i].data() == classVal)
        {
            return i;
        }
    }
    return -1;
}

void AppSocket::on_ConnectToServer()
{
    socket->connectToHost(QHostAddress(IP),APPport);
    while(true)
    {
        bool ok = socket->waitForConnected(INT_MAX);
        if(ok)
        {
            break;
        }
    }
    qDebug()<<"连接到了服务端";
    emit successCon();
}

void AppSocket::on_readedMessage()
{
    QByteArray data = socket->readAll();
    qDebug()<<data;
    QJsonParseError error;
    QJsonDocument dc = QJsonDocument::fromJson(data,&error);
    QJsonObject obj = dc.object();
    QString classVal;
    for(auto Jit = obj.begin(); Jit != obj.end(); Jit++)
    {
        if(Jit.key() == "class")
        {
            classVal = Jit.value().toString();
        }
    }
    int switchNum = num(classVal);
    switch (switchNum) {
    case -1: break;
    case 0: emit SignUpResult(data); break;
    case 1: break;
    case 2: break;
    case 3: break;
    case 4: break;
    case 5: break;
    case 6: break;
    case 7: break;
    case 8: break;
    case 9: break;
    case 10: break;
    case 11: break;
    case 12: break;
    case 13: break;
    case 14: break;
    case 15: break;
    case 16: break;
    case 17: break;
    }
}
