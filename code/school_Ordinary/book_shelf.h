#ifndef BOOK_SHELF_H
#define BOOK_SHELF_H

#include <QWidget>

namespace Ui {
class Book_Shelf;
}

class Book_Shelf : public QWidget
{
    Q_OBJECT

public:
    explicit Book_Shelf(QWidget *parent = nullptr);
    ~Book_Shelf();
    QWidget* getbookshelf(QString bookpictureaddr,QString bookname,QString author, QString ISBN,QString publisher,QString currentstatus);
private:
    Ui::Book_Shelf *ui;
};

#endif // BOOK_SHELF_H
