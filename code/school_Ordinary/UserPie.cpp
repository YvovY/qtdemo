#include "UserPie.h"
#include"config.h"
#include <QChart>
#include<QFile>
#include <QChartView>
#include <QGridLayout>
#include <QPieSeries>
#include<QDebug>
#include <QFileInfo>
#include <QTextCodec>
QT_CHARTS_USE_NAMESPACE
UserPie::UserPie(QWidget *parent)
    : QWidget{parent}
{
}

void UserPie::paintEvent(QPaintEvent *e)
{
    if(!map.isEmpty())
    {
        return;
    }
    Q_UNUSED(e);
    //    读取文件
    QString dir = TempUrl +"/"+Username+".txt";
    qDebug()<<dir;
    QFileInfo info(dir);
    if(info.exists())
    {
        QFile file(dir);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream linetext(& file);
        QTextCodec *codec = QTextCodec::codecForName("UTF-8");//或者"GBK",不分大小写
        QTextCodec::setCodecForLocale(codec);
        linetext.setCodec(codec);
        QStringList ulist;
        while(!linetext.atEnd())
        {
            QString str = linetext.readLine();
            ulist<< str;
        }
        qDebug()<< ulist;
        file.close();
        for(int i = 0; i < ulist.size(); i++)
        {
            QString booktype = ulist[i].section("#",2,2);
            double score = ulist[i].section("#",3,3).toDouble();
            map[booktype] += score;
        }
        double total = 0.0;
        for(auto i = map.begin(); i != map.end(); i++)
        {
            qDebug()<<i.key()<<" "<<i.value();
            total += i.value();
        }
        //画图
        QPieSeries* series = new QPieSeries();
        for(auto i = map.begin(); i != map.end(); i++)
        {
            QString legends = i.key() +":"+ QString::number(i.value() / total * 100)+ "%";
            series->append(legends,i.value());
        }
        QChart* chart = new QChart();
        chart->legend()->setVisible(true);
        chart->legend()->setAlignment(Qt::AlignmentFlag::AlignRight);
        chart->addSeries(series);
        chart->setTitle("人物画像");
        QChartView* view = new QChartView(chart);
        view->setRenderHint(QPainter::Antialiasing);
        QGridLayout *mainLayout = new QGridLayout;
        mainLayout->addWidget(view, 0, 0);
        setLayout(mainLayout);
    }
    else
    {
        qDebug()<<"不存在文件："<<Username+".txt";
    }
}

void UserPie::drawuserpie()
{
    this->update();
}
