#ifndef SC_H
#define SC_H

#include <QFile>
#include <QObject>
#include<QTcpServer>
#include<QTcpSocket>
#include <QTimer>

/*

服务端类：用来向客户端类发送文件

*/
class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);
    bool startlistern();
    void stoplistern();
    void fileinfo(QString filepath);
    void sendfile();
    void sendData();
    int getPort();
signals:
    QString message(QString text);
    int processnum(int num);
    int getserverport(int port);
private slots:
    void on_connection();
private:
    int port;
    QTimer* sotimer;
    QString filename;
    qint64 filesize;
    qint64 sendsize;
    QFile file;
    QTcpServer* ss;
    QTcpSocket* so;
};


/*

客户端类：接受服务端发送过来的文件

*/
class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QObject *parent = nullptr);
    void connectToServer(QString IP, int port);
signals:
    QString message(QString text);
    int processnum(int num);
private:
    bool isStart;
    QString filename;
    qint64 filesize;
    qint64 recvsize;
    QFile file;
    QTcpSocket* co;
};


/*

  压缩/解压缩类

*/
class filezip : public QObject
{
    Q_OBJECT
public:
    explicit filezip(QObject *parent = nullptr);
    void zip(QString savepath, QString inputpath);
    void unzip(QString outputpath, QString zippath);
signals:
    void complete();
};


// 服务类：接受数据
class Server2 : public QObject
{
    Q_OBJECT
public:
    explicit Server2(QObject *parent = nullptr);
    void setFileSaveAddr(QString fileSavePath);
    int getPort();
signals:
    QString getFilepath(QString filepath);
    void disconnects();
    QString message(QString text);
private slots:
    void on_newConnection();
    void on_readClientData();
private:
    QString fileSavePath;
    int port;
    bool isStart = true;
    QString filename;
    qint64 filesize;
    qint64 recvsize;
    QFile file;
    QTcpServer* ss;
    QTcpSocket* so;
};

// 客户端类：发送数据
class Client2 : public QObject
{
    Q_OBJECT
public:
    explicit Client2(QObject *parent = nullptr);
    void setPort(int port);
    void connectToServer(QString IP);
    void setFilePath(QString filePath);
signals:
    QString message(QString text);
    void disconnects();
private slots:
    void sendData();
    void on_Connect();
private:
    int port;
    QTimer* sotimer;
    QString filename;
    QString filePath;
    qint64 filesize;
    qint64 sendsize;
    QFile file;
    QTcpSocket* co;
};

#endif // SC_H
