#ifndef USERPIE_H
#define USERPIE_H

#include <QMap>
#include <QWidget>
class UserPie : public QWidget
{
    Q_OBJECT
private:
    QMap<QString, double> map;
public:
    explicit UserPie(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent* e);
public slots:
    void drawuserpie();

};

#endif // USERPIE_H
