#ifndef LOAD_H
#define LOAD_H

#include"SC.h"
#include <QWidget>
#include <QThread>
#include <QSqlDataBase>
#include"signup.h"
#include"masterui.h"
#include"forgotpassword.h"
#include"AppSocket.h"
QT_BEGIN_NAMESPACE
namespace Ui { class load; }
QT_END_NAMESPACE

class load : public QWidget
{
    Q_OBJECT
private:
    QTimer* timer;
    QSqlDatabase bookdb;
    AppSocket* appS;
    Ui::load *ui;//ui文件指针
    SignUp* signup;//注册页面指针
    MasterUI* masterUI;//主界面指针
    ForgotPassword* forgot;//忘记密码界面指针
public:
    load(QWidget *parent = nullptr);//构造函数
    ~load();//析构函数
private:
    void initOtherUI(); //初始化其他窗口构造函数
    void initFunc();//调用其他函数
    void initConnection();//链接曹与信号
    void wallPapers();//登陆页面右边的图片窗口
    void logos();//logo图片
    void avatars();//头像
    void recvDBfile();//获取数据库文件
    void recvBookPicture();//获取书本图片
signals://信号

private slots://槽函数
    void on_successCon();
    void on_LoadShow();//显示窗口
    void on_btSignUp_clicked();//点击注册按钮
    void on_btSignIn_clicked();//点击登录按钮
    void on_ForgotPassword_clicked();//点击忘记密码按钮

};
#endif // LOAD_H
