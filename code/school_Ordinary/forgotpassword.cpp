#include "forgotpassword.h"
#include "ui_forgotpassword.h"

ForgotPassword::ForgotPassword(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ForgotPassword)
{
    ui->setupUi(this);
//    置顶：
    Qt::WindowFlags flags = this->windowFlags();
    this->setWindowFlags(flags | Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint);
//    禁止其他窗口响应事件
    this->setWindowModality(Qt::ApplicationModal);
}

ForgotPassword::~ForgotPassword()
{
    delete ui;
}

void ForgotPassword::on_AccountNumber_toggled(bool checked)
{
    if(checked)
    {
        ui->inner->setCurrentIndex(0);
    }
}


void ForgotPassword::on_VerificationCode_toggled(bool checked)
{
    if(checked)
    {
        ui->inner->setCurrentIndex(1);
    }
}


void ForgotPassword::on_newPasswd_toggled(bool checked)
{
    if(checked)
    {
        ui->inner->setCurrentIndex(2);
    }
}

