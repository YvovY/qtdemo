QT       += core gui sql network charts
#QT       += testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
CONFIG += resources_big
CONFIG(debug, debug|release){
    MOC_DIR = "$$OUT_PWD/tmp/debug/.moc"
    OBJECTS_DIR = "$$OUT_PWD/tmp/debug/.obj"
    UI_DIR = "$$OUT_PWD/tmp/debug/.ui"
    RCC_DIR = "$$OUT_PWD/tmp/debug/.qrc"
}
CONFIG(release, debug|release){
    MOC_DIR = "$$OUT_PWD/tmp/release/.moc"
    OBJECTS_DIR = "$$OUT_PWD/tmp/release/.obj"
    UI_DIR = "$$OUT_PWD/tmp/release/.ui"
    RCC_DIR = "$$OUT_PWD/tmp/release/.qrc"
}

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AppCon.cpp \
    AppSocket.cpp \
    SC.cpp \
    UserPie.cpp \
    book_shelf.cpp \
    config.cpp \
    forgotpassword.cpp \
    main.cpp \
    load.cpp \
    masterui.cpp \
    signup.cpp

HEADERS += \
    AppCon.h \
    AppSocket.h \
    SC.h \
    UserPie.h \
    book_shelf.h \
    config.h \
    filetransfer.hpp \
    forgotpassword.h \
    load.h \
    masterui.h \
    signup.h

FORMS += \
    book_shelf.ui \
    forgotpassword.ui \
    load.ui \
    masterui.ui \
    signup.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ordinary.qrc

LIBS += -lws2_32

RC_ICONS = 13.ico
