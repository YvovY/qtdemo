#include "book_shelf.h"
#include "ui_book_shelf.h"

Book_Shelf::Book_Shelf(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Book_Shelf)
{
    ui->setupUi(this);
}

Book_Shelf::~Book_Shelf()
{
    delete ui;
}

QWidget *Book_Shelf::getbookshelf(QString bookpictureaddr, QString bookname, QString author, QString ISBN, QString publisher, QString currentstatus)
{
    QPixmap icon(bookpictureaddr);
    ui->bookpicture->setPixmap(icon.scaled(QSize(ui->bookpicture->width(),ui->bookpicture->height())));
    ui->bookName->setText(bookname);
    ui->author->setText(author);
    ui->ISBNcode->setText(ISBN);
    ui->publisher->setText(publisher);
    ui->status->setText(currentstatus);
    return this;
}
