#include "signup.h"
#include "ui_signup.h"
#include"config.h"
#include <QCloseEvent>
#include <QDir>
#include <QFileDialog>
#include <QPixmap>
#include<QDebug>
#include <QJsonObject>
#include <QMessageBox>
#include <QJsonArray>
#include <QJsonDocument>
#include <QRegExp>
#include <QRegExpValidator>
#include<QFile>
#include <QThread>
#include <QTimer>
#include"SC.h"
SignUp::SignUp(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SignUp)
{
    ui->setupUi(this);
    logoPicture();
    connect(appS,&AppSocket::SignUpResult,this,&SignUp::on_SignUpResult);
}

SignUp::~SignUp()
{
    delete ui;
}

void SignUp::logoPicture()
{
    ui->Logo->setText("");
    int w = ui->Logo->width();
    int h = ui->Logo->height();
    QPixmap pix;
    pix.load(logo_picture_address);
    ui->Logo->setPixmap(pix.scaled(w,h));
}

void SignUp::closeEvent(QCloseEvent*)
{
    if(this->close())
    {
        emit LoadShow();
    }
}

void SignUp::on_AvtarPicture1_clicked()
{
    QString dir = QDir::homePath();
    QString title = "选择头像";
    QString filter = "图片(*.jpg *.jpeg *.png);;";
    avpicture = QFileDialog::getOpenFileName(this,title,dir,filter);
    if(!avpicture.isEmpty())
    {
        qDebug()<<avpicture;
        QPixmap icon(avpicture);
        ui->AvtarPicture1->setText("");
        ui->AvtarPicture1->setIconSize(QSize(ui->AvtarPicture1->width()-10,ui->AvtarPicture1->height()-10));
        icon = icon.scaled(ui->AvtarPicture1->width(), ui->AvtarPicture1->height());
        ui->AvtarPicture1->setIcon(icon);
    }
}


void SignUp::on_UserName_textChanged(const QString &arg1)
{
    username = arg1;
    qDebug()<<arg1;
}


void SignUp::on_Passwd_textChanged(const QString &arg1)
{
    passwd = arg1;
    qDebug()<<arg1;
}


void SignUp::on_PhoneNumber_textChanged(const QString &arg1)
{
    phonenum = arg1;
    qDebug()<<arg1;
}


void SignUp::on_Ciphertext_textChanged(const QString &arg1)
{
    miwen = arg1;
    qDebug()<<arg1;
}

bool SignUp::IsValidPhoneNumber(const QString & phoneNum)
{
    QRegExp regx("^[1][3-9][0-9]{9}$");
    QRegExpValidator regs(regx, 0);
    QString pNum = phoneNum;
    int pos = 0;
    QValidator::State res = regs.validate(pNum, pos);
    if (QValidator::Acceptable == res) {
        return true;
    }
    else {
        return false;
    }
}

int SignUp::selectNum(QString &classVal)
{
    QStringList classList;
    classList<<"successAddNewUser"<<"exists";
    for(int i = 0; i < classList.size(); i++)
    {
        if(classList[i].data() == classVal)
        {
            return i;
        }
    }
    return -1;
}

void SignUp::sendavtarPicture(QJsonObject obj)
{

    QJsonObject::Iterator jsonIt;
    int Val =-1;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "port")
        {
            Val = jsonIt.value().toInt();
        }
    }
    qDebug()<<IP<<" "<<Val<<" "<<cpfile;
    Client2* CS = new Client2;
    CS->setPort(Val);
    CS->setFilePath(cpfile);
    CS->connectToServer(IP);
    QEventLoop* el = new QEventLoop();
    connect(CS, &Client2::disconnects, el, &QEventLoop::quit);
    el->exec();
    ui->messagess->setText("账号注册成功！\n请回到登陆页面登录。");
}

void SignUp::existUser()
{
    ui->messagess->setText("抱歉，此账户已存在\n请使用别的号码进行注册！");
}

void SignUp::on_SignUpResult(QByteArray data)
{
    QJsonParseError error;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(data,&error);
    if(error.error != QJsonParseError::NoError)
    {
        qDebug()<<"Json文件读取错误["<<error.errorString()<<"]";
    }
    QJsonObject obj = jsonDocument.object();
    QJsonObject::Iterator jsonIt;
    QString classVal =NULL;
    for(jsonIt = obj.begin(); jsonIt != obj.end(); jsonIt++)
    {
        if(jsonIt.key() == "command")
        {
            classVal = jsonIt.value().toString();
        }
    }
    int switchNum = selectNum(classVal);
    switch(switchNum)
    {
    case -1: break;
    case 0: sendavtarPicture(obj); break;
    case 1: existUser(); break;
    case 2: break;
    case 3: break;
    case 4: break;
    case 5: break;
    case 6: break;
    case 7: break;
    case 8: break;
    case 9: break;
    case 10: break;
    case 11: break;
    case 12: break;
    }
}

void SignUp::on_SignUps_clicked()
{
    if(avpicture==NULL || username==NULL || passwd==NULL || phonenum==NULL || miwen==NULL)
    {
        QMessageBox::information(this,"提示","请完成所有要完成的步骤！");
        return;
    }
    if(IsValidPhoneNumber(phonenum) == false)
    {
        QMessageBox::information(this,"提示","请输入正确的手机号！");
        return;
    }
    QJsonObject obj;
    obj["class"] = "SignUp";
    obj["command"] = "SinNewUser";
    obj["phonenum"] = phonenum;
    obj["username"] = username;
    obj["passwd"] = passwd;
    obj["miwen"] = miwen;
    QFileInfo info(avpicture);
    obj["avpicture"] = phonenum+"User"+info.fileName();
    cpfile = avtarpictureUrl+"/"+phonenum+"User"+info.fileName();
    if(QFile::copy(avpicture,cpfile))
    {
        qDebug()<<"["<<__FUNCTION__<<"]"<<"复制成功";
    }
    QJsonDocument doc;
    doc.setObject(obj);
    QByteArray data = doc.toJson();
    appS->SocketWrited(data);
    ui->messagess->setText("正在注册中，请稍候！\n请不要关闭此页面，注册完成之后会提示您。");
}

