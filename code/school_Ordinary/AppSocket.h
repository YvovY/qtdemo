#ifndef APPSOCKET_H
#define APPSOCKET_H

#include <QObject>
#include<QTcpSocket>
#include"config.h"
class AppSocket : public QObject
{
    Q_OBJECT
private:
    QTcpSocket* socket;
    QStringList classList;
public:
    ~AppSocket();
    static AppSocket& GetInitAppSocket();
    void SocketWrited(const QByteArray &data);
private:
    explicit AppSocket(QObject *parent = nullptr);
    int num(QString classVal);
public slots:
    void on_ConnectToServer();
    void on_readedMessage();
signals:
    void SignUpResult(QByteArray data);
    void successCon();
};

#endif // APPSOCKET_H
