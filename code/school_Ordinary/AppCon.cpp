#include "AppCon.h"
#include"config.h"
#include <QDir>
#include<QDebug>
AppCon::AppCon(QObject *parent)
    : QObject{parent}
{

}

void AppCon::serch_dir(QString AppDir)
{
    QDir dir;
    QStringList folderList;
    folderList<<"AppDataDir"<<"ConfigDir"<<"DataBaseDir"<<"PictureDir"<<"UserDataDir"<<"Temp"<<"avtarpictureDir"<<"bookpictureDir";
    bool ok = dir.cd(AppDir);
    if(ok)
    {
        qDebug()<<"cd到"<<AppDir;
        applicationUrl = AppDir;
    }else
    {
        qDebug()<<"无法cd到AppDir";
        return;
    }
    ok = dir.exists(folderList[0]);
    if(ok)
    {
        qDebug()<<"此文件存在 ";
        appDataUrl = AppDir+"/"+folderList[0];
        qDebug()<<appDataUrl;
    }else
    {
        dir.mkdir(folderList[0]);
        qDebug()<<"此文件不存在，已创建";
        appDataUrl = AppDir+"/"+folderList[0];
        qDebug()<<appDataUrl;
    }
    ok = dir.cd(appDataUrl);
    if(ok)
    {
        qDebug()<<"cd到"<<appDataUrl;
    }else
    {
        qDebug()<<"无法cd到AppDir";
    }
    ok = dir.exists(folderList[1]);
    if(ok)
    {
        qDebug()<<"此文件存在 ";
        configDataUrl = appDataUrl+"/"+folderList[1];
        qDebug()<<configDataUrl;
    }else
    {
        dir.mkdir(folderList[1]);
        qDebug()<<"此文件不存在，已创建";
        configDataUrl = appDataUrl+"/"+folderList[1];
        qDebug()<<configDataUrl;
    }
    ok = dir.exists(folderList[2]);
    if(ok)
    {
        qDebug()<<"此文件存在 ";
        databaseUrl = appDataUrl+"/"+folderList[2];
        qDebug()<<databaseUrl;
    }else
    {
        dir.mkdir(folderList[2]);
        qDebug()<<"此文件不存在，已创建";
        databaseUrl = appDataUrl+"/"+folderList[2];
        qDebug()<<databaseUrl;
    }
    ok = dir.exists(folderList[3]);
    if(ok)
    {
        qDebug()<<"此文件存在 ";
        pictureUrl = appDataUrl+"/"+folderList[3];
        qDebug()<<pictureUrl;
    }else
    {
        dir.mkdir(folderList[3]);
        qDebug()<<"此文件不存在，已创建";
        pictureUrl = appDataUrl+"/"+folderList[3];
        qDebug()<<pictureUrl;
    }
    ok = dir.exists(folderList[4]);
    if(ok)
    {
        qDebug()<<"此文件存在 ";
        userDataUrl = appDataUrl+"/"+folderList[4];
        qDebug()<<userDataUrl;
    }else
    {
        dir.mkdir(folderList[4]);
        qDebug()<<"此文件不存在，已创建";
        userDataUrl = appDataUrl+"/"+folderList[4];
        qDebug()<<userDataUrl;
    }
    ok = dir.exists(folderList[5]);
    if(ok)
    {
        qDebug()<<"此文件存在 ";
        TempUrl = appDataUrl+"/"+folderList[5];
        qDebug()<<TempUrl;
    }else
    {
        dir.mkdir(folderList[5]);
        qDebug()<<"此文件不存在，已创建";
        TempUrl = appDataUrl+"/"+folderList[5];
        qDebug()<<TempUrl;
    }
    ok = dir.cd(pictureUrl);
    if(ok)
    {
        qDebug()<<"cd到"<<pictureUrl;
    }else
    {
        qDebug()<<"无法cd到此目录";
        return;
    }
    ok = dir.exists(folderList[6]);
    if(ok)
    {
        avtarpictureUrl = pictureUrl+"/"+folderList[6];
        qDebug()<<"此目录存在"<<avtarpictureUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[6]);
        avtarpictureUrl = pictureUrl+"/"+folderList[6];
        qDebug()<<avtarpictureUrl;
    }
    ok = dir.exists(folderList[7]);
    if(ok)
    {
        bookpictureUrl = pictureUrl+"/"+folderList[7];
        qDebug()<<"此目录存在"<<bookpictureUrl;
    }else{
        qDebug()<<"此目录不存在，正在创建";
        dir.mkdir(folderList[7]);
        bookpictureUrl = pictureUrl+"/"+folderList[7];
        qDebug()<<bookpictureUrl;
    }
}
